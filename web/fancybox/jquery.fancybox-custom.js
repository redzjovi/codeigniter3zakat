function fancybox_initialize()
{
    // https://stackoverflow.com/questions/22611174/combining-dropzone-js-with-fancybox-js-to-give-a-fullscreen-view-of-uploaded-pho
    $('.upload_fancybox').fancybox();
}

fancybox_initialize();
