<ol class="breadcrumb">
    <li class="breadcrumb-item active"><?= lang('answers'); ?></li>
</ol>

<?= anchor('backend/answers/create', lang('create')); ?>
<br /><br />
<table class="dynatable table table-responsive">
    <thead class="thead-inverse">
        <tr>
            <th><?= lang('no'); ?></th>
            <th><?= lang('question'); ?></th>
            <th><?= lang('answer_by'); ?></th>
            <th><?= lang('content'); ?></th>
            <th><?= lang('created_at'); ?></th>
            <th><?= lang('actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php if ($answers) : ?>
            <?php foreach ($answers as $i => $answer) : ?>
                <tr>
                    <td><?= $i + 1; ?></td>
                    <td><?= $answer->question->title; ?></td>
                    <td><?= $answer->user->name; ?></td>
                    <td><?= $answer->content; ?></td>
                    <td><?= $answer->created_at; ?></td>
                    <td>
                        <?= anchor('backend/answers/update/'.$answer->id, lang('update')); ?> |
                        <?= anchor('backend/answers/delete/'.$answer->id, lang('delete'), ['onclick' => "return confirm('".lang('are_you_sure_you_want_to_delete_this')."')"]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>

<script>
$(document).ready(function() {
    $('.dynatable').dynatable();
});
</script>
