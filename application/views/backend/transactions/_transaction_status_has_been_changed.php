<?php
$ci =& get_instance();
$ci->load->model('Pages_Model');
$page = $ci->Pages_Model->where('template', 'transaction_status_has_been_changed')->set_cache('page_transaction_status_has_been_changed')->get();
?>

<?= $page->content ?><br />

<?= lang('transaction') ?>: <?= $transaction->id ?><br />
<?= lang('status') ?>: <?= lang($transaction->status).' '.strtolower(lang('become')).' '.lang($data['status']) ?><br />
<?= lang('updated_at') ?>: <?= date('Y-m-d H:i:s') ?><br />
