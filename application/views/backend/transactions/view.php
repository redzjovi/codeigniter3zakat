<ol class="breadcrumb">
    <li class="breadcrumb-item"><?= anchor('backend/transactions', lang('transactions')); ?></li>
    <li class="breadcrumb-item active"><?= (isset($transaction) ? lang('view') : lang('create')); ?></li>
</ol>

<table class="table table-bordered">
    <tbody>
        <tr>
            <td><?= lang('transaction'); ?></td>
            <td><?= $transaction->id; ?></td>
        </tr>
        <tr>
            <td><?= lang('user'); ?></td>
            <td><?= $transaction->user->name; ?></td>
        </tr>
        <tr>
            <td><?= lang('upload'); ?></td>
            <td>
                <a class="upload_fancybox" href="<?= base_url($transaction->upload); ?>">
                    <img height="100" src="<?= base_url($transaction->upload); ?>" style="object-fit: cover;" width="100" />
                </a>
            </td>
        </tr>
        <tr>
            <td><?= lang('total'); ?></td>
            <td><?= $Products_Model->currency.' '.number_format($transaction->total); ?></td>
        </tr>
        <tr>
            <td><?= lang('status'); ?></td>
            <td class="form-inline">
                <?= form_open(); ?>
                    <?= form_hidden('id', $transaction->id); ?>
                    <?= form_dropdown(['class' => 'form-control form-control-sm', 'name' => 'status', 'options' => $Transactions_Model->get_status_options(), 'selected' => $transaction->status]); ?>
                    <?= form_submit(['class' => 'btn btn-sm btn-success', 'name' => 'update_status', 'value' => lang('update')]); ?>
                <?= form_close(); ?>
            </td>
        </tr>
        <tr>
            <td><?= lang('created_at'); ?></td>
            <td><?= $transaction->created_at; ?></td>
        </tr>
    </tbody>
</table>

<?php if ($transaction_details) : ?>
    <?php $no = 1; ?>
    <table class="table table-bordered table-sm">
        <thead>
            <tr class="table-active">
                <th><?= lang('no'); ?></th>
                <th><?= lang('name'); ?></th>
                <th class="text-right"><?= lang('quantity'); ?></th>
                <th class="text-right"><?= lang('price'); ?></th>
                <th class="text-right"><?= lang('total'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($transaction_details as $i => $transaction_detail) : ?>
                <?php $options = json_decode($transaction_detail->options, true); ?>
                <tr>
                    <td><?= $no; ?></td>
                    <td><?= $transaction_detail->product->name; ?></td>
                    <td align="right">
                        <?= number_format($transaction_detail->quantity); ?>
                        <?php ($options['unit'] ? '('.$options['unit'].')' : ''); ?>
                    </td>
                    <td align="right"><?= $Products_Model->currency.' '.number_format($transaction_detail->price); ?></td>
                    <td align="right"><?= $Products_Model->currency.' '.number_format($transaction_detail->sub_total); ?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
