<ol class="breadcrumb">
    <li class="breadcrumb-item"><?= anchor('backend/transactions', lang('transactions')); ?></li>
</ol>

<table class="dynatable table table-responsive">
    <thead class="thead-inverse">
        <tr>
            <th><?= lang('no'); ?></th>
            <th><?= lang('user'); ?></th>
            <th><?= lang('total'); ?></th>
            <th><?= lang('status'); ?></th>
            <th><?= lang('created_at'); ?></th>
            <th><?= lang('actions'); ?></th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <?= form_open('', ['method' => 'get']); ?>
            <td></td>
            <td></td>
            <td></td>
            <td><?= form_dropdown(['class' => 'form-control form-control-sm', 'name' => 'status', 'options' => ['' => ''] + $Transactions_Model->get_status_options(), 'selected' => $Transactions_Model->status]); ?></td>
            <td></td>
            <td><?= form_submit(['class' => 'btn btn-success btn-sm', 'value' => lang('filter')]); ?></td>
            <?= form_close(); ?>
        </tr>
    </tfoot>
    <tbody>
        <?php if ($transactions) : ?>
            <?php foreach ($transactions as $i => $transaction) : ?>
                <tr>
                    <td><?= $i + 1; ?></td>
                    <td><?= $transaction->user->name; ?></td>
                    <td><?= number_format($transaction->total); ?></td>
                    <td><label class="btn btn-sm <?= $Transactions_Model->get_status_color_class($transaction->status); ?>"><?= lang($transaction->status); ?></label></td>
                    <td><?= $transaction->created_at; ?></td>
                    <td>
                        <?= anchor('backend/transactions/view/'.$transaction->id, lang('view')); ?> |
                        <?= anchor('backend/transactions/delete/'.$transaction->id, lang('delete'), ['onclick' => "return confirm('".lang('are_you_sure_you_want_to_delete_this')."')"]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>

<script>
$(document).ready(function() {
    $('.dynatable').dynatable();
});
</script>
