<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= lang('transactions'); ?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?= $transactions_unpaid_count; ?></h3>
                        <p><?= lang('unpaid'); ?></p>
                    </div>
                    <a class="small-box-footer" href="<?= site_url('backend/transactions?status='.$Transactions_Model->status_unpaid); ?>"><?= lang('view'); ?></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?= $transactions_waiting_count; ?></h3>
                        <p><?= lang('waiting'); ?></p>
                    </div>
                    <a class="small-box-footer" href="<?= site_url('backend/transactions?status='.$Transactions_Model->status_waiting); ?>"><?= lang('view'); ?></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?= $transactions_paid_count; ?></h3>
                        <p><?= lang('paid'); ?></p>
                    </div>
                    <a class="small-box-footer" href="<?= site_url('backend/transactions?status='.$Transactions_Model->status_paid); ?>"><?= lang('view'); ?></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?= $transactions_expired_count; ?></h3>
                        <p><?= lang('expired'); ?></p>
                    </div>
                    <a class="small-box-footer" href="<?= site_url('backend/transactions?status='.$Transactions_Model->status_expired); ?>"><?= lang('view'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><?= lang('questions_and_answers'); ?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?= $questions_count; ?></h3>
                        <p><?= lang('questions'); ?></p>
                    </div>
                    <a class="small-box-footer" href="<?= site_url('backend/questions'); ?>"><?= lang('view'); ?></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?= $answers_count; ?></h3>
                        <p><?= lang('answers'); ?></p>
                    </div>
                    <a class="small-box-footer" href="<?= site_url('backend/answers'); ?>"><?= lang('view'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
