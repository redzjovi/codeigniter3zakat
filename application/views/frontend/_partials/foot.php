<?php
$ci =& get_instance();
$ci->load->model(['Categories_Model', 'News_Model', 'Pages_Model']);
$categories = $ci->Categories_Model->order_by('name')->set_cache('footer_categories')->get_all();
$recent_news = $ci->News_Model->order_by('created_at', 'DESC')->limit(5)->set_cache('footer_recent_news')->get_all();
$page = $ci->Pages_Model->where('template', 'footer')->set_cache('footer')->get();
?>

<footer class="page-footer center-on-small-only">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4"><?= $page->content; ?></div>
            <hr class="w-100 clearfix d-sm-none">

            <div class="col-md-4">
                <?php if ($recent_news) : ?>
                    <h5 class="title mb-3"><b><?= lang('recent_news') ?></b></h5>
                    <ol>
                        <?php foreach ($recent_news as $news) : ?>
                            <li><a href="<?= site_url($news->slug) ?>"><?= $news->title ?></a></li>
                        <?php endforeach; ?>
                    </ol>
                <?php endif; ?>
            </div>
            <hr class="w-100 clearfix d-sm-none">

            <div class="col-md-4">
                <?php if ($categories) : ?>
                    <h5 class="title mb-3"><b><?= lang('categories') ?></b></h5>
                    <ul>
                        <?php foreach ($categories as $category) : ?>
                            <li><a href="<?= site_url('category/'.$category->slug) ?>"><?= $category->name ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container-fluid">&copy; <?= date('Y').' '.lang('copyright'); ?>: <?= anchor('', $_ENV['APPLICATION_NAME']); ?></div>
    </div>
</footer>
<div align="center">Page rendered in <strong>{elapsed_time}</strong> seconds. Memory Usage: <strong>{memory_usage}</strong>. <?= (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : ''; ?></div>
