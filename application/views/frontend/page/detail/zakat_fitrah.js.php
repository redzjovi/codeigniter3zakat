<script>
$(document).ready(function () {
    $('.quantity').keyup(function () {
        var index = $(this).attr('data-index');
        var sub_total = $('#price_'+index).val() * $(this).val();
        $('#sub_total_'+index).val(sub_total); $('#sub_total_'+index+'_format').html( numeral(sub_total).format() );
    });
});
</script>
