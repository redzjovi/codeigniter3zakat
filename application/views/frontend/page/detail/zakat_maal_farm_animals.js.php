<script>
$(document).ready(function () {
    var $animals_owned = $('.animals_owned');
    var $calculate = $('#calculate');
    var $sub_total = $('.sub_total');
    var $total = $('#total');
    var $total_format = $('#total_format');

    function animals_owned_keyup()
    {
        $animals_owned.each(function () {
            var id = $(this).attr('data-id');
            $.get(site_url+'api/zakat/maal_farm_animals', { code: $(this).attr('data-code'), owned: $(this).val() }, function (response) {
                $('#total_'+id).val(response.total); $('#total_'+id+'_format').html( numeral(response.total).format() );
                $('#description_'+id).val(response.description); $('#description_'+id+'_format').html(response.description);
                total_calculate();
            });
        });
    }

    function total_calculate()
    {
        var total = 0;
        $sub_total.each(function () {
            total += parseInt($(this).val());
        });
        $total.val(total); $total_format.html( numeral(total).format() );
    }

    $animals_owned.each(function () {
        $(this).keyup(function () { animals_owned_keyup() });
    });
    $calculate.click(function () { animals_owned_keyup(); });
});
</script>
