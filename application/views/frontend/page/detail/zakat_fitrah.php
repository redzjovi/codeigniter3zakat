<div class="row">

    <!--Main column-->
    <div class="col-md-12">
        <!--Post-->
        <div class="post-wrapper wow fadeIn" data-wow-delay="0.2s">
            <!--Post data-->
            <h1 class="h1-responsive font-bold"><?= anchor('page/'.$page->slug, $page->title); ?></h1>

            <!--Featured image -->
            <?php if ($page->image) : ?>
                <div align="center" class="z-depth-1-half">
                    <img class="feature-image img-fluid" src="<?= base_url($page->image); ?>" />
                </div>
            <?php endif; ?>

            <!--Post excerpt-->
            <p><?= $page->content; ?></p>
        </div>
        <!--/.Post-->

        <hr />
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <?= form_open(current_url_with_params().'#zakat_form', ['id' => 'zakat_form']); ?>
        <table class="table table-bordered table-hover table-sm">
            <tbody>
                <?php foreach ($products as $i => $product) : ?>
                    <tr>
                        <td width="50%">
                            <?= form_hidden('product_id[]', $product->id); ?>
                            <?= form_hidden('product_name[]', $product->name); ?>
                            <?= $product->name; ?>
                        </td>
                        <td align="right"><?= $Products_Model->currency.' '.number_format($product->price); ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('zakat_fitrah'); ?></td>
                        <td align="right">
                            <?= form_input(['id' => 'price_'.$i, 'name' => 'price[]', 'type' => 'hidden', 'value' => $this->zakat_library->fitrah($product->price)]); ?>
                            <?= $Products_Model->currency.' '.number_format($this->zakat_library->fitrah($product->price)); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('quantity').' '.lang('muzakki'); ?></td>
                        <td>
                            <div class="form-xs md-form">
                                <?= form_label(lang('quantity'), 'quantity'); ?>
                                <?= form_input(['class' => 'form-control text-right quantity', 'data-index' => $i, 'id' => 'quantity_'.$i, 'name' => 'quantity[]', 'required' => true, 'type' => 'number', 'value' => set_value('quantity[]')]); ?>
                                <?= form_error('quantity[]', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('total_zakat'); ?></td>
                        <td align="right">
                            <?= $Products_Model->currency; ?>
                            <span id="sub_total_<?= $i; ?>_format"><?= (set_value('sub_total['.$i.']') ? number_format(set_value('sub_total['.$i.']')) : 0); ?></span>
                            <?= form_input(['id' => 'sub_total_'.$i, 'name' => 'sub_total[]', 'type' => 'hidden', 'value' => set_value('sub_total[]')]); ?>
                            <?= form_error('sub_total[]', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <td colspan="2">
                    <button class="btn btn-warning pull-right" name="add_to_cart" type="submit" value="add_to_cart">
                        <i class="fa fa-pencil"></i> <?= lang('add_to_cart'); ?>
                    </button>
                </td>
            </tfoot>
        </table>
        <?= form_close(); ?>
    </div>
</div>

<?php $this->load->view('frontend/page/detail/zakat_fitrah.js.php'); ?>
