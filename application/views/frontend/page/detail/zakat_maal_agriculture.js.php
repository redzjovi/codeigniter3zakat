<script>
$(document).ready(function () {
    var $calculate = $('#calculate');
    var $harvest = $('#harvest');
    var $irrigation_cost_type = $('#irrigation_cost_type');
    var $must_zakat = $('#must_zakat');
    var $must_zakat_select = $('#must_zakat_select');
    var $total = $('#total');
    var $total_format = $('#total_format');

    function harvest_keyup()
    {
        $.get(site_url+'api/zakat/get_must_zakat_maal_agriculture', { total_harvest: $harvest.val() }, function (response) {
            $must_zakat.val(response.must_zakat); $must_zakat_select.val(response.must_zakat);
            total_calculate();
        });
    }

    function irrigation_cost_type_change()
    {
        $.get(site_url+'api/zakat/get_must_zakat_maal_agriculture', { total_harvest: $harvest.val() }, function (response) {
            $must_zakat.val(response.must_zakat); $must_zakat_select.val(response.must_zakat);
            total_calculate();
        });
    }

    function total_calculate()
    {
        $.get(site_url+'api/zakat/maal_agriculture', { irrigation_cost_type: $irrigation_cost_type.val(), total: $harvest.val() }, function (response) {
            $total.val(response.total); $total_format.html( numeral(response.total).format() );
        });
    }

    $calculate.click(function () { harvest_keyup(); });
    $harvest.keyup(function () { harvest_keyup(); });
    $irrigation_cost_type.change(function () { irrigation_cost_type_change(); });
});
</script>
