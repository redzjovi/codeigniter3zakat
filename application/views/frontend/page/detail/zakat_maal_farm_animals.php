<div class="row">

    <!--Main column-->
    <div class="col-md-12">
        <!--Post-->
        <div class="post-wrapper wow fadeIn" data-wow-delay="0.2s">
            <!--Post data-->
            <h1 class="h1-responsive font-bold"><?= anchor('page/'.$page->slug, $page->title); ?></h1>

            <!--Featured image -->
            <?php if ($page->image) : ?>
                <div align="center" class="z-depth-1-half">
                    <img class="feature-image img-fluid" src="<?= base_url($page->image); ?>" />
                </div>
            <?php endif; ?>

            <!--Post excerpt-->
            <p><?= $page->content; ?></p>
        </div>
        <!--/.Post-->

        <hr />
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <?= form_open(current_url_with_params().'#zakat_form', ['id' => 'zakat_form']); ?>
        <table class="table table-bordered table-hover table-sm">
            <tbody>
                <tr class="table-success"><td colspan="2"><?= lang('farm_animals_owned'); ?></td></tr>
                <?php foreach ($products as $i => $product) : ?>
                    <tr>
                        <td width="50%"><?= lang($product->code); ?></td>
                        <td>
                            <?= form_input(['name' => 'product_id[]', 'type' => 'hidden', 'value' => $product->id]); ?>
                            <?= form_input(['name' => 'product_name[]', 'type' => 'hidden', 'value' => $product->name]); ?>
                            <div class="form-xs md-form">
                                <?= form_label(lang($product->code)); ?>
                                <?= form_input(['class' => 'animals_owned text-right', 'data-code' => $product->code, 'data-id' => $product->id, 'min' => 0, 'name' => 'animals_owned[]', 'type' => 'number', 'value' => set_value('animals_owned['.$i.']', 0)]); ?>
                                <?= form_error('animals_owned[]', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr class="table-success"><td colspan="2"><?= lang('zakat'); ?></td></tr>
                <?php foreach ($products as $i => $product) : ?>
                    <tr>
                        <td><?= lang($product->code); ?></td>
                        <td align="right">
                            <?= form_input(['class' => 'sub_total', 'id' => 'total_'.$product->id, 'name' => 'quantity[]', 'type' => 'hidden', 'value' => set_value('quantity['.$i.']', 0)]); ?>
                            <span id="total_<?= $product->id; ?>_format"><?= number_format(set_value('quantity['.$i.']', 0)); ?></span>
                            <?= form_error('quantity[]', '<h6 class="text-danger"><small>', '</small></h6>'); ?>

                            <?= form_input(['id' => 'description_'.$product->id, 'name' => 'description[]', 'type' => 'hidden', 'value' => set_value('description['.$i.']')]); ?>
                            <h6 class="text-danger">
                                <small>
                                    <label id="description_<?= $product->id; ?>_format"><?= set_value('description['.$i.']'); ?></label>
                                </small>
                            </h6>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr class="table-warning">
                    <td><?= lang('total_zakat'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'total', 'name' => 'total', 'type' => 'hidden', 'value' => set_value('total', 0)]); ?>
                        <span id="total_format"><?= number_format(set_value('total', 0)); ?></span>
                        <?= form_error('total', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <td align="right" colspan="2">
                    <button class="btn btn-success invisible" id="calculate" type="button">
                        <i class="fa fa-calculator"></i> <?= lang('calculate'); ?>
                    </button>
                    <button class="btn btn-warning" name="add_to_cart" type="submit" value="add_to_cart">
                        <i class="fa fa-pencil"></i> <?= lang('add_to_cart'); ?>
                    </button>
                </td>
            </tfoot>
        </table>
        <?= form_close(); ?>
    </div>
</div>

<?php $this->load->view('frontend/page/detail/zakat_maal_farm_animals.js.php'); ?>
