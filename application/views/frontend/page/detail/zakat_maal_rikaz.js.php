<script>
$(document).ready(function () {
    var $calculate = $('#calculate');
    var $treasure_value = $('#treasure_value');
    var $total = $('#total');
    var $total_format = $('#total_format');

    function total_calculate()
    {
        $.get(site_url+'api/zakat/maal_rikaz', { total: $treasure_value.val() }, function (response) {
            $total.val(response.total); $total_format.html( numeral(response.total).format() );
        });
    }

    $calculate.click(function () { total_calculate(); });
    $treasure_value.keyup(function () { total_calculate(); });
});
</script>
