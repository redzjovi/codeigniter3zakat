<div class="row">

    <!--Main column-->
    <div class="col-md-12">
        <!--Post-->
        <div class="post-wrapper wow fadeIn" data-wow-delay="0.2s">
            <!--Post data-->
            <h1 class="h1-responsive font-bold"><?= anchor('page/'.$page->slug, $page->title); ?></h1>

            <!--Featured image -->
            <?php if ($page->image) : ?>
                <div align="center" class="z-depth-1-half">
                    <img class="feature-image img-fluid" src="<?= base_url($page->image); ?>" />
                </div>
            <?php endif; ?>

            <!--Post excerpt-->
            <p><?= $page->content; ?></p>
        </div>
        <!--/.Post-->

        <hr />
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <?= form_open(current_url_with_params().'#zakat_form', ['id' => 'zakat_form']); ?>
        <table class="table table-bordered table-hover table-sm">
            <tbody>
                <tr class="table-success"><td colspan="2"><?= lang('assets_you_have_owned'); ?></td></tr>
                <tr>
                    <td width="50%"><?= lang('gold'); ?></td>
                    <td>
                        <div class="form-xs md-form">
                            <?= form_label(lang('gold').' (gr)', 'gold_owned'); ?>
                            <?= form_input(['class' => 'form-control text-right', 'id' => 'gold_owned', 'name' => 'gold_owned', 'required' => true, 'type' => 'number', 'value' => set_value('gold_owned', 0)]); ?>
                            <?= form_error('gold_owned', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('silver'); ?></td>
                    <td>
                        <div class="form-xs md-form">
                            <?= form_label(lang('silver').' (gr)', 'silver_owned'); ?>
                            <?= form_input(['class' => 'form-control text-right', 'id' => 'silver_owned', 'name' => 'silver_owned', 'required' => true, 'type' => 'number', 'value' => set_value('silver_owned', 0)]); ?>
                            <?= form_error('silver_owned', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                        </div>
                    </td>
                </tr>
                <tr class="table-success"><td colspan="2"><?= lang('zakat_in_gold_or_silver'); ?></td></tr>
                <tr>
                    <td><?= lang('gold'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'gold_quantity', 'name' => 'quantity[]', 'type' => 'hidden', 'value' => set_value('quantity[]', 0)]); ?>
                        <span id="gold_quantity_format">0</span> (gr)
                    </td>
                </tr>
                <tr>
                    <td><?= lang('silver'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'silver_quantity', 'name' => 'quantity[]', 'type' => 'hidden', 'value' => set_value('quantity[]', 0)]); ?>
                        <span id="silver_quantity_format">0</span> (gr)
                    </td>
                </tr>
                <tr class="table-success"><td colspan="2"><?= lang('zakat_if_paid_with_money'); ?></td></tr>
                <?php foreach ($products as $i => $product) : ?>
                    <tr>
                        <td>
                            <?= form_input(['name' => 'product_id[]', 'type' => 'hidden', 'value' => $product->id]); ?>
                            <?= form_input(['name' => 'product_name[]', 'type' => 'hidden', 'value' => $product->name]); ?>
                            <?= $product->name; ?> (gr)
                        </td>
                        <td align="right">
                            <?php $product_name = strtolower($product->name) == 'emas' ? 'gold' : 'silver'; ?>
                            <?= form_input(['id' => $product_name.'_price', 'name' => 'price[]', 'type' => 'hidden', 'value' => $product->price]); ?>
                            <?= $Products_Model->currency.' '.number_format($product->price); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td><?= lang('zakat_gold'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'gold_sub_total', 'type' => 'hidden']); ?>
                        <?= $Products_Model->currency; ?> <span id="gold_sub_total_format">0</span>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('zakat_silver'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'silver_sub_total', 'type' => 'hidden']); ?>
                        <?= $Products_Model->currency; ?> <span id="silver_sub_total_format">0</span>
                    </td>
                </tr>
                <tr class="table-warning">
                    <td><?= lang('total_zakat'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'total', 'name' => 'total', 'type' => 'hidden', 'value' => set_value('total', 0)]); ?>
                        <?= $Products_Model->currency; ?> <span id="total_format"><?= number_format(set_value('total', 0)); ?></span>
                        <?= form_error('total', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <td align="right" colspan="2">
                    <button class="btn btn-success" id="calculate" type="button">
                        <i class="fa fa-calculator"></i> <?= lang('calculate'); ?>
                    </button>
                    <button class="btn btn-warning" name="add_to_cart" type="submit" value="add_to_cart">
                        <i class="fa fa-pencil"></i> <?= lang('add_to_cart'); ?>
                    </button>
                </td>
            </tfoot>
        </table>
        <?= form_close(); ?>
    </div>
</div>

<?php $this->load->view('frontend/page/detail/zakat_maal_gold_and_silver.js.php'); ?>
