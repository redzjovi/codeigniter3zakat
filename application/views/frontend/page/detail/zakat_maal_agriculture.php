<div class="row">

    <!--Main column-->
    <div class="col-md-12">
        <!--Post-->
        <div class="post-wrapper wow fadeIn" data-wow-delay="0.2s">
            <!--Post data-->
            <h1 class="h1-responsive font-bold"><?= anchor('page/'.$page->slug, $page->title); ?></h1>

            <!--Featured image -->
            <?php if ($page->image) : ?>
                <div align="center" class="z-depth-1-half">
                    <img class="feature-image img-fluid" src="<?= base_url($page->image); ?>" />
                </div>
            <?php endif; ?>

            <!--Post excerpt-->
            <p><?= $page->content; ?></p>
        </div>
        <!--/.Post-->

        <hr />
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <?= form_open(current_url_with_params().'#zakat_form', ['id' => 'zakat_form']); ?>
        <?= form_input(['name' => 'product_id', 'type' => 'hidden', 'value' => $product->id]); ?>
        <?= form_input(['name' => 'product_name', 'type' => 'hidden', 'value' => $product->name]); ?>
        <?= form_input(['name' => 'quantity', 'type' => 'hidden', 'value' => 1]); ?>
        <table class="table table-bordered table-hover table-sm">
            <tbody>
                <tr class="table-success"><td colspan="2"><?= lang('seeds_and_fruits'); ?></td></tr>
                <tr>
                    <td width="50%"><?= lang('harvest').' (kg)'; ?></td>
                    <td>
                        <div class="form-xs md-form">
                            <?= form_label(lang('harvest').' (kg)'); ?>
                            <?= form_input(['class' => 'form-control text-right', 'id' => 'harvest', 'min' => 0, 'name' => 'harvest', 'required' => true, 'type' => 'number', 'value' => set_value('harvest', 0)]); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('must_zakat'); ?>?</td>
                    <td align="right">
                        <?= form_input(['id' => 'must_zakat', 'name' => 'must_zakat', 'type' => 'hidden', 'value' => set_value('must_zakat', 0)]); ?>
                        <?php $must_zakat_options = $this->zakat_library->get_duty_options(); ?>
                        <?= form_dropdown(['class' => 'form-control', 'id' => 'must_zakat_select', 'options' => $must_zakat_options, 'disabled' => 'disabled', 'value' => set_value('must_zakat', 0)]); ?>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('irrigation_cost_type'); ?></td>
                    <td align="right">
                        <?php $irrigation_cost_type_options = $this->zakat_library->get_irrigation_cost_type_options(); ?>
                        <?= form_dropdown(['class' => 'form-control', 'id' => 'irrigation_cost_type', 'name' => 'irrigation_cost_type', 'options' => $irrigation_cost_type_options, 'required' => true, 'value' => set_value('irrigation_cost_type', 0)]); ?>
                    </td>
                </tr>
                <tr class="table-warning">
                    <td><?= lang('total_zakat'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'total', 'name' => 'total', 'type' => 'hidden', 'value' => set_value('total', 0)]); ?>
                        <span id="total_format"><?= number_format(set_value('total', 0)); ?></span> (kg)
                        <?= form_error('total', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <td align="right" colspan="2">
                    <button class="btn btn-success invisible" id="calculate" type="button">
                        <i class="fa fa-calculator"></i> <?= lang('calculate'); ?>
                    </button>
                    <button class="btn btn-warning" name="add_to_cart" type="submit" value="add_to_cart">
                        <i class="fa fa-pencil"></i> <?= lang('add_to_cart'); ?>
                    </button>
                </td>
            </tfoot>
        </table>
        <?= form_close(); ?>
    </div>
</div>

<?php $this->load->view('frontend/page/detail/zakat_maal_agriculture.js.php'); ?>
