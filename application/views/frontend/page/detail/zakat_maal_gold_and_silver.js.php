<script>
$(document).ready(function () {
    var $calculate = $('#calculate');
    var $gold_owned = $('#gold_owned'); // @param
    var $gold_price = $('#gold_price');
    var $gold_quantity = $('#gold_quantity'); // @return
    var $gold_quantity_format = $('#gold_quantity_format'); // @return
    var $gold_sub_total = $('#gold_sub_total');
    var $gold_sub_total_format = $('#gold_sub_total_format');

    var $silver_owned = $('#silver_owned'); // @param
    var $silver_price = $('#silver_price');
    var $silver_quantity = $('#silver_quantity'); // @return
    var $silver_quantity_format = $('#silver_quantity_format'); // @return
    var $silver_sub_total = $('#silver_sub_total');
    var $silver_sub_total_format = $('#silver_sub_total_format');

    var $total = $('#total');
    var $total_format = $('#total_format');

    function gold_owned_keyup()
    {
        $.get(site_url+'api/zakat/maal_gold_and_silver', { owned: $gold_owned.val(), type: 'gold' }, function (response) {
            $gold_quantity.val(response.quantity); $gold_quantity_format.html(response.quantity);
            var gold_sub_total = parseInt($gold_quantity.val()) * parseInt($gold_price.val());
            $gold_sub_total.val(gold_sub_total); $gold_sub_total_format.html( numeral(gold_sub_total).format() );
            total_calculate();
        });
    }

    function silver_owned_keyup()
    {
        $.get(site_url+'api/zakat/maal_gold_and_silver', { owned: $silver_owned.val(), type: 'silver' }, function (response) {
            $silver_quantity.val(response.quantity); $silver_quantity_format.html(response.quantity);
            var silver_sub_total = parseInt($silver_quantity.val()) * parseInt($silver_price.val());
            $silver_sub_total.val(silver_sub_total); $silver_sub_total_format.html( numeral(silver_sub_total).format() );
            total_calculate();
        });
    }

    function total_calculate()
    {
        var total = parseInt($gold_sub_total.val()) + parseInt($silver_sub_total.val());
        $total.val(total); $total_format.html( numeral(total).format() );
    }

    $calculate.click(function () {
        gold_owned_keyup();
        silver_owned_keyup();
    });
    // $gold_owned.keyup(function () { gold_owned_keyup(); });
    // $silver_owned.keyup(function () { silver_owned_keyup(); });
});
</script>
