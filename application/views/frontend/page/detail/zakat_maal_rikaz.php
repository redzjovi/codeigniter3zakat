<div class="row">

    <!--Main column-->
    <div class="col-md-12">
        <!--Post-->
        <div class="post-wrapper wow fadeIn" data-wow-delay="0.2s">
            <!--Post data-->
            <h1 class="h1-responsive font-bold"><?= anchor('page/'.$page->slug, $page->title); ?></h1>

            <!--Featured image -->
            <?php if ($page->image) : ?>
                <div align="center" class="z-depth-1-half">
                    <img class="feature-image img-fluid" src="<?= base_url($page->image); ?>" />
                </div>
            <?php endif; ?>

            <!--Post excerpt-->
            <p><?= $page->content; ?></p>
        </div>
        <!--/.Post-->

        <hr />
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <?= form_open(current_url_with_params().'#zakat_form', ['id' => 'zakat_form']); ?>
        <table class="table table-bordered table-hover table-sm">
            <tbody>
                <tr class="table-success"><td colspan="2"><?= lang('treasure_value'); ?></td></tr>
                <tr>
                    <td width="50%"><?= lang('treasure_value'); ?></td>
                    <td>
                        <?= form_input(['name' => 'product_id', 'type' => 'hidden', 'value' => $product->id]); ?>
                        <?= form_input(['name' => 'product_name', 'type' => 'hidden', 'value' => $product->name]); ?>
                        <div class="form-xs md-form">
                            <?= form_label(lang('treasure_value')); ?>
                            <?= form_input(['class' => 'text-right', 'id' => 'treasure_value', 'min' => 0, 'name' => 'treasure_value', 'type' => 'number', 'value' => set_value('treasure_value[', 0)]); ?>
                            <?= form_error('treasure_value', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                        </div>
                    </td>
                </tr>
                <tr class="table-warning">
                    <td><?= lang('total_zakat'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'total', 'name' => 'total', 'type' => 'hidden', 'value' => set_value('total', 0)]); ?>
                        <?= $Products_Model->currency; ?> <span id="total_format"><?= number_format(set_value('total', 0)); ?></span>
                        <?= form_error('total', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <td align="right" colspan="2">
                    <button class="btn btn-success invisible" id="calculate" type="button">
                        <i class="fa fa-calculator"></i> <?= lang('calculate'); ?>
                    </button>
                    <button class="btn btn-warning" name="add_to_cart" type="submit" value="add_to_cart">
                        <i class="fa fa-pencil"></i> <?= lang('add_to_cart'); ?>
                    </button>
                </td>
            </tfoot>
        </table>
        <?= form_close(); ?>
    </div>
</div>

<?php $this->load->view('frontend/page/detail/zakat_maal_rikaz.js.php'); ?>
