<script>
$(document).ready(function () {
    var $accounts_receivable = $('#accounts_receivable');
    var $assets_total = $('#assets_total');
    var $calculate = $('#calculate');
    var $assets_total_format = $('#assets_total_format');
    var $cash_and_deposit = $('#cash_and_deposit');
    var $debit = $('#debit');
    var $differences_between_assets_and_obligations = $('#differences_between_assets_and_obligations');
    var $differences_between_assets_and_obligations_format = $('#differences_between_assets_and_obligations_format');
    var $nishab_total = $('#nishab_total'); // @return
    var $nishab_total_format = $('#nishab_total_format'); // @return
    var $nishab_type = $('#nishab_type'); // @param
    var $obligations_total = $('#obligations_total');
    var $obligations_total_format = $('#obligations_total_format');
    var $stock_and_other_valuable = $('#stock_and_other_valuable');
    var $total = $('#total');
    var $total_format = $('#total_format');

    function assets_total_calculate()
    {
        var assets_total = parseInt($accounts_receivable.val()) + parseInt($cash_and_deposit.val()) + parseInt($stock_and_other_valuable.val());
        $assets_total.val(assets_total); $assets_total_format.html( numeral(assets_total).format() );
        differences_between_assets_and_obligations_calculate();
    }

    function differences_between_assets_and_obligations_calculate()
    {
        var differences_between_assets_and_obligations = parseInt($assets_total.val()) - parseInt($obligations_total.val());
        $differences_between_assets_and_obligations.val(differences_between_assets_and_obligations); $differences_between_assets_and_obligations_format.html( numeral(differences_between_assets_and_obligations).format() );
        total_calculate();
    }

    function nishab_change()
    {
        var nishab_type = $nishab_type.val();
        var price = $('#'+nishab_type+'_price').val();
        $.get(site_url+'api/zakat/get_nishab_total', { nishab_type: nishab_type, price: price }, function (response) {
            $nishab_total.val(response.nishab_total); $nishab_total_format.html( numeral(response.nishab_total).format() );
            total_calculate();
        });
    }

    function obligations_total_calculate()
    {
        var obligations_total = parseInt($debit.val());
        $obligations_total.val(obligations_total); $obligations_total_format.html( numeral(obligations_total).format() );
        differences_between_assets_and_obligations_calculate();
    }

    function total_calculate()
    {
        var total = parseInt($assets_total.val()) - parseInt($obligations_total.val());
        if (total > 0) {
            $.get(site_url+'api/zakat/maal_money', { nishab_total: $nishab_total.val(), total: total }, function (response) {
                $total.val(response.total); $total_format.html( numeral(response.total).format() );
            });
        }
    }

    // $accounts_receivable.keyup(function () { assets_total_calculate(); });
    $calculate.click(function () {
        nishab_change();
        assets_total_calculate();
        obligations_total_calculate();
    });
    // $cash_and_deposit.keyup(function () { assets_total_calculate(); });
    // $debit.keyup(function () { obligations_total_calculate(); });
    // $nishab_type.change(function () { nishab_change(); });
    // $stock_and_other_valuable.keyup(function () { assets_total_calculate(); });
});
</script>
