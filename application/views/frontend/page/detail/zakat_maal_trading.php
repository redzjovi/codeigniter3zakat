<div class="row">

    <!--Main column-->
    <div class="col-md-12">
        <!--Post-->
        <div class="post-wrapper wow fadeIn" data-wow-delay="0.2s">
            <!--Post data-->
            <h1 class="h1-responsive font-bold"><?= anchor('page/'.$page->slug, $page->title); ?></h1>

            <!--Featured image -->
            <?php if ($page->image) : ?>
                <div align="center" class="z-depth-1-half">
                    <img class="feature-image img-fluid" src="<?= base_url($page->image); ?>" />
                </div>
            <?php endif; ?>

            <!--Post excerpt-->
            <p><?= $page->content; ?></p>
        </div>
        <!--/.Post-->

        <hr />
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <?= form_open(current_url_with_params().'#zakat_form', ['id' => 'zakat_form']); ?>
        <?= form_input(['name' => 'product_id', 'type' => 'hidden', 'value' => $product->id]); ?>
        <?= form_input(['name' => 'product_name', 'type' => 'hidden', 'value' => $product->name]); ?>
        <table class="table table-bordered table-hover table-sm">
            <tbody>
                <tr class="table-success"><td colspan="2"><?= lang('nishab'); ?></td></tr>
                <tr>
                    <td width="50%"><?= lang('nishab_to_be_used'); ?></td>
                    <td>
                        <div class="md-form">
                            <?php $nishab_options = $this->zakat_library->get_nishab_options(); ?>

                            <?= form_input(['id' => 'nishab_type', 'name' => 'nishab_type', 'type' => 'hidden', 'value' => 'gold']); ?>
                            <?= lang('gold'); ?>
                            <?= form_error('gold_owned', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                        </div>
                    </td>
                </tr>
                <?php foreach ($products as $i => $product) : ?>
                    <?php if (strtolower($product->name) != 'emas') { continue; } ?>
                    <tr>
                        <td><?= $product->name; ?> (gr)</td>
                        <td align="right">
                            <?php $product_name = strtolower($product->name) == 'emas' ? 'gold' : 'silver'; ?>
                            <?= form_input(['id' => $product_name.'_price', 'name' => 'price[]', 'type' => 'hidden', 'value' => $product->price]); ?>
                            <?= $Products_Model->currency.' '.number_format($product->price); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td><?= lang('total_nishab'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'nishab_total', 'name' => 'nishab_total', 'type' => 'hidden', 'value' => set_value('nishab_total', 0)]); ?>
                        <?= $Products_Model->currency; ?> <span id="nishab_total_format"><?= number_format(set_value('nishab_total', 0)); ?></span>
                    </td>
                </tr>
                <tr class="table-success"><td colspan="2"><?= lang('assets_you_have_owned'); ?></td></tr>
                <tr>
                    <td><?= lang('cash_and_deposit'); ?></td>
                    <td>
                        <div class="form-xs md-form">
                            <?= form_label(lang('cash_and_deposit')); ?>
                            <?= form_input(['class' => 'form-control text-right', 'id' => 'cash_and_deposit', 'min' => 0, 'name' => 'cash_and_deposit', 'required' => true, 'type' => 'number', 'value' => set_value('cash_and_deposit', 0)]); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('stock_of_items'); ?></td>
                    <td>
                        <div class="form-xs md-form">
                            <?= form_label(lang('stock_of_items')); ?>
                            <?= form_input(['class' => 'form-control text-right', 'id' => 'stock_of_items', 'min' => 0, 'name' => 'stock_of_items', 'required' => true, 'type' => 'number', 'value' => set_value('stock_of_items', 0)]); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('accounts_receivable'); ?></td>
                    <td>
                        <div class="form-xs md-form">
                            <?= form_label(lang('accounts_receivable')); ?>
                            <?= form_input(['class' => 'form-control text-right', 'id' => 'accounts_receivable', 'min' => 0, 'name' => 'accounts_receivable', 'required' => true, 'type' => 'number', 'value' => set_value('accounts_receivable', 0)]); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('total_assets'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'assets_total', 'name' => 'assets_total', 'type' => 'hidden', 'value' => set_value('assets_total', 0)]); ?>
                        <?= $Products_Model->currency; ?> <span id="assets_total_format"><?= number_format(set_value('assets_total', 0)); ?></span>
                    </td>
                </tr>
                <tr class="table-success"><td colspan="2"><?= lang('obligations'); ?></td></tr>
                <tr>
                    <td><?= lang('debit'); ?></td>
                    <td>
                        <div class="form-xs md-form">
                            <?= form_label(lang('debit')); ?>
                            <?= form_input(['class' => 'form-control text-right', 'id' => 'debit', 'min' => 0, 'name' => 'debit', 'required' => true, 'type' => 'number', 'value' => set_value('debit', 0)]); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('total_obligations'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'obligations_total', 'name' => 'obligations_total', 'type' => 'hidden', 'value' => set_value('obligations_total', 0)]); ?>
                        <?= $Products_Model->currency; ?> <span id="obligations_total_format"><?= number_format(set_value('obligations_total', 0)); ?></span>
                    </td>
                </tr>
                <tr class="table-success"><td colspan="2"><?= lang('zakat'); ?></td></tr>
                <tr>
                    <td><?= lang('differences_between_assets_and_obligations'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'differences_between_assets_and_obligations', 'name' => 'differences_between_assets_and_obligations', 'type' => 'hidden', 'value' => set_value('differences_between_assets_and_obligations', 0)]); ?>
                        <?= $Products_Model->currency; ?> <span id="differences_between_assets_and_obligations_format"><?= number_format(set_value('differences_between_assets_and_obligations', 0)); ?></span>
                    </td>
                </tr>
                <tr class="table-warning">
                    <td><?= lang('total_zakat'); ?></td>
                    <td align="right">
                        <?= form_input(['id' => 'total', 'name' => 'total', 'type' => 'hidden', 'value' => set_value('total', 0)]); ?>
                        <?= $Products_Model->currency; ?> <span id="total_format"><?= number_format(set_value('total', 0)); ?></span>
                        <?= form_error('total', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <td colspan="2">
                    <div class="row">
                        <div class="col-md-8"></div>
                        <div align="right" class="col-md-4">
                            <button class="btn btn-success invisible" id="calculate" type="button">
                                <i class="fa fa-calculator"></i> <?= lang('calculate'); ?>
                            </button>
                            <button class="btn btn-warning" name="add_to_cart" type="submit" value="add_to_cart">
                                <i class="fa fa-pencil"></i> <?= lang('add_to_cart'); ?>
                            </button>
                        </div>
                    </div>
                </td>
            </tfoot>
        </table>
        <?= form_close(); ?>
    </div>
</div>

<?php $this->load->view('frontend/page/detail/zakat_maal_trading.js.php'); ?>
