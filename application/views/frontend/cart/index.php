<?php if ($contents) : ?>
    <?php $no = 1; ?>
    <table class="table table-bordered table-sm">
        <thead>
            <tr class="table-active">
                <th><?= lang('no'); ?></th>
                <th><?= lang('name'); ?></th>
                <th class="text-right"><?= lang('quantity'); ?></th>
                <th class="text-right"><?= lang('price'); ?></th>
                <th class="text-right"><?= lang('total'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contents as $i => $content) : ?>
                <tr>
                    <td><?= $no; ?></td>
                    <td>
                        <?= $content['name']; ?>
                        <a href="<?= site_url('cart/delete/'.$i); ?>"><i class="fa fa-times-circle"></i></a>
                    </td>
                    <td align="right">
                        <?= number_format($content['qty']); ?>
                        <?= ($content['options']['unit'] ? '('.$content['options']['unit'].')' : ''); ?>
                    </td>
                    <td align="right"><?= $Products_Model->currency.' '.number_format($content['price']); ?></td>
                    <td align="right"><?= $Products_Model->currency.' '.number_format($content['subtotal']); ?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="justify-content-end row">
        <div class="col-md-4">
            <table class="table table-bordered table-sm">
                <tbody>
                    <tr>
                        <td align="right" width="50%"><?= lang('total'); ?>:</td>
                        <td align="right"><?= $Products_Model->currency.' '.number_format($this->cart->total()); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="justify-content-end row">
        <div class="col-md-2">
            <a class="btn btn-warning pull-right" href="<?= site_url('cart/checkout'); ?>" onclick="return confirm('<?= lang('are_you_sure_you_want_to_checkout'); ?>')"><?= lang('checkout'); ?></a>
        </div>
    </div>
<?php else : ?>
    <?= lang('your_cart_is_empty'); ?>
<?php endif; ?>
