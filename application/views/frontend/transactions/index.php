<ol class="breadcrumb">
    <li class="breadcrumb-item active"><?= lang('transactions'); ?></li>
</ol>

<table class="table table-bordered table-responsive">
    <thead class="thead-inverse">
        <tr>
            <th><?= lang('no'); ?></th>
            <th><?= lang('user'); ?></th>
            <th><?= lang('total'); ?></th>
            <th><?= lang('status'); ?></th>
            <th><?= lang('created_at'); ?></th>
            <th><?= lang('actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php if ($transactions) : ?>
            <?php foreach ($transactions as $i => $transaction) : ?>
                <tr>
                    <td><?= $i + 1; ?></td>
                    <td><?= $transaction->user->name; ?></td>
                    <td><?= number_format($transaction->total); ?></td>
                    <td><label class="btn btn-sm <?= $Transactions_Model->get_status_color_class($transaction->status); ?>"><?= lang($transaction->status); ?></label></td>
                    <td><?= $transaction->created_at; ?></td>
                    <td><a class="btn btn-warning" href="<?= site_url('transaction/detail/'.$transaction->id); ?>"><?= lang('view'); ?></a></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>
