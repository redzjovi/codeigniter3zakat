<script>
Dropzone.autoDiscover = false;
$(document).ready(function() {
    var $upload_dropzone = $('#upload_dropzone');
    var $id = $('#id');


    $upload_dropzone = new Dropzone('#upload_dropzone', {
        addRemoveLinks: true,
        maxFiles: 1,
        url: site_url+'api/transactions/upload',

        complete: function (file) {
            $('.dz-progress').hide();
            var anchor_to_fancybox = $('<a></a>').attr('class', 'upload_fancybox').attr('href', file.dataURL);
            $(file.previewElement).find('.dz-details').append(anchor_to_fancybox).click(function () { $(this).find('a').click(); });
            fancybox_initialize();
        },
        error: function(file, errorMessage) { this.removeFile(file); $('#upload_dropzone_error').html(errorMessage); },
        init: function() {
            var myDropzone = this;

            if ($id.length) {
                $.getJSON(site_url+'api/transactions/index/'+$id.val(), function(data) {
                    if (! $.isEmptyObject(data) && data.transaction.upload != '') {
                        var mockFile = { accepted: true, name: data.transaction.upload_name, size: data.transaction.upload_size, dataURL: base_url+data.transaction.upload };

                        myDropzone.emit('addedfile', mockFile);
                        myDropzone.createThumbnailFromUrl(
                            mockFile,
                            myDropzone.options.thumbnailWidth,
                            myDropzone.options.thumbnailHeight,
                            myDropzone.options.thumbnailMethod,
                            true,
                            function(thumbnail) { myDropzone.emit('thumbnail', mockFile, thumbnail); myDropzone.emit('complete', mockFile); }
                        );
                        myDropzone.files.push(mockFile);
                    }
                });
            }
        },
        removedfile: function(file) { $('#upload').val(''); $('#upload_dropzone_error').html(''); file.previewElement.remove(); },
        renameFilename: function (filename) { return new Date().getTime() + '_' + filename; },
        success: function(file, responseText) { $('#upload').val(responseText.file.temp_full_path); },
    });
});
</script>
