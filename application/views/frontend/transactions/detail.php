<ol class="breadcrumb">
    <li class="breadcrumb-item"><?= anchor('transactions', lang('transactions')); ?></li>
    <li class="breadcrumb-item active"><?= $transaction->id; ?></li>
    <li class="breadcrumb-item active"><?= lang('view'); ?></li>
</ol>

<?= form_open(); ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td><?= lang('transaction'); ?></td>
                    <td>
                        <?= form_input(['id' => 'id', 'name' => 'id', 'type' => 'hidden', 'value' => $transaction->id]); ?>
                        <?= $transaction->id; ?>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('user'); ?></td>
                    <td><?= $transaction->user->name; ?></td>
                </tr>
                <tr>
                    <td><?= lang('total'); ?></td>
                    <td><?= $Products_Model->currency.' '.number_format($transaction->total); ?></td>
                </tr>
                <tr>
                    <td><?= lang('status'); ?></td>
                    <td><?= lang($transaction->status); ?></td>
                </tr>
                <tr>
                    <td><?= lang('created_at'); ?></td>
                    <td><?= $transaction->created_at; ?></td>
                </tr>
            </tbody>
        </table>

        <?php if ($transaction_details) : ?>
            <?php $no = 1; ?>
            <table class="table table-bordered table-sm">
                <thead>
                    <tr class="table-active">
                        <th><?= lang('no'); ?></th>
                        <th><?= lang('name'); ?></th>
                        <th class="text-right"><?= lang('quantity'); ?></th>
                        <th class="text-right"><?= lang('price'); ?></th>
                        <th class="text-right"><?= lang('total'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($transaction_details as $i => $transaction_detail) : ?>
                        <?php $options = json_decode($transaction_detail->options, true); ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $transaction_detail->product->name; ?></td>
                            <td align="right">
                                <?= number_format($transaction_detail->quantity); ?>
                                <?php ($options['unit'] ? '('.$options['unit'].')' : ''); ?>
                            </td>
                            <td align="right"><?= $Products_Model->currency.' '.number_format($transaction_detail->price); ?></td>
                            <td align="right"><?= $Products_Model->currency.' '.number_format($transaction_detail->sub_total); ?></td>
                        </tr>
                        <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>

<?php if (in_array($transaction->status, [$Transactions_Model->status_unpaid, $Transactions_Model->status_waiting])) : ?>
    <div class="form-group">
        <div class="card">
            <h3 class="card-header success-color white-text"><?= lang('how_to_pay'); ?></h3>
            <div class="card-body">
                <?php $this->load->view('frontend/cart/_how_to_pay'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= form_label(lang('upload')); ?>
        <?= form_input(['name' => 'upload', 'id' => 'upload', 'type' => 'hidden', 'value' => (isset($transaction) ? set_value('upload', $transaction->upload) : set_value('upload'))]); ?>
        <?= form_error('upload', '<div class="text-danger">', '</div>'); ?>

        <div class="dropzone" id="upload_dropzone"></div>
        <div class="text-danger" id="upload_dropzone_error"></div>
    </div>
    <div align="center" class="form-group">
        <?= form_submit('submit_receipt', lang('submit_receipt'), ['class' => 'btn btn-warning']); ?>
    </div>

    <?php $this->load->view('frontend/transactions/detail.js.php'); // view ?>
<?php endif; ?>

<?= form_close(); ?>
