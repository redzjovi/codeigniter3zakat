<div class="card card-sign-in text-center">
    <?= form_open(current_url_with_params()); ?>
    <div class="card-header purple darken-2 white-text"><?= lang('reset_password'); ?></div>
    <?php $this->load->view('frontend/_partials/messages') // view ?>
    <div class="card-body">
        <div class="md-form">
            <?= form_label(lang('password'), 'password'); ?>
            <?= form_input(['class' => 'form-control', 'id' => 'password', 'name' => 'password', 'required' => true, 'type' => 'password', 'value' => set_value('password')]); ?>
            <?= form_error('password', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
        </div>
    </div>
    <div class="card-footer">
        <?= form_submit(['class' => 'btn btn-block btn-warning', 'name' => 'reset_password', 'value' => lang('reset')]); ?>
    </div>
    <div class="card-footer">
        <?= anchor('', lang('back')); ?>
    </div>
    <?= form_close(); ?>
</div>
