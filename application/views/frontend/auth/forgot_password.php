<div class="card card-sign-in text-center">
    <?= form_open(current_url_with_params()); ?>
    <div class="card-header purple darken-2 white-text"><?= lang('forgot_password'); ?></div>
    <?php $this->load->view('frontend/_partials/messages') // view ?>
    <div class="card-body">
        <div class="md-form"><?= lang('please_enter_your_email_address_and_we_will_send_a_link_to_reset_your_password') ?></div>
        <div class="md-form">
            <?= form_label(lang('email'), 'email'); ?>
            <?= form_input(['class' => 'form-control', 'id' => 'email', 'name' => 'email', 'required' => true, 'type' => 'email', 'value' => set_value('email')]); ?>
            <?= form_error('email', '<h6 class="text-danger"><small>', '</small></h6>'); ?>
        </div>
    </div>
    <div class="card-footer">
        <?= form_submit(['class' => 'btn btn-block btn-warning', 'name' => 'forgot_password', 'value' => lang('submit')]); ?>
    </div>
    <div class="card-footer">
        <?= anchor('auth/sign_in', lang('back')); ?>
    </div>
    <?= form_close(); ?>
</div>
