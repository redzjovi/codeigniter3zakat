<div class="row">

    <!--Main column-->
    <div class="col-md-8">
        <!--Post-->
        <div class="post-wrapper wow fadeIn" data-wow-delay="0.2s">
            <!--Post data-->
            <h1 class="h1-responsive font-bold"><?= anchor($news->slug, $news->title); ?></h1>
            <h6>
                <?= $news->category ? anchor('category/'.$news->category->slug, $news->category->name).', ' : ''; ?>
                <?= date('d M Y', strtotime($news->created_at)); ?>
            </h6>

            <!--Featured image -->
            <?php if ($news->image) : ?>
                <div align="center" class="z-depth-1-half">
                    <img class="feature-image img-fluid" src="<?= base_url($news->image); ?>" />
                </div>
            <?php endif; ?>

            <!--Post excerpt-->
            <p><?= $news->content; ?></p>
        </div>
        <!--/.Post-->

        <hr />
    </div>

    <!--Sidebar-->
    <div class="col-md-4">
        <div class="sticky widget-wrapper wow fadeIn" data-wow-delay="0.4s">
            <h4 class="font-bold"><?= lang('categories'); ?>:</h4>
            <br />
            <div class="list-group">
                <?php if ($categories) : ?>
                    <?php foreach ($categories as $category) : ?>
                        <a class="list-group-item" href="<?= site_url('category/'.$category->slug) ?>"><?= $category->name ?></a>
                    <?php endforeach ?>
                <?php endif; ?>
            </div>

            <hr />
        </div>
    </div>
    <!--/.Sidebar-->
</div>

<?php $this->load->view('frontend/news/index.js.php'); // view ?>
