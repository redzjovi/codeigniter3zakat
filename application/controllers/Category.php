<?php

class Category extends Frontend_Controller
{
    protected $limit = 5;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Categories_Model', 'News_Model']);
    }

    public function index($slug = '')
    {
        ($category = $this->Categories_Model->where('slug', $slug)->get()) ?: show_404();

        $vars['categories'] = $this->Categories_Model->order_by('name')->get_all();
        $vars['category'] = $category;
        $vars['news'] = $this->News_Model->with_category()->where('category_id', $category->id)->order_by('created_at', 'DESC')->limit($this->limit, $this->input->get('offset'))->get_all();

        $config = $this->pagination->bootstrap_material;
        $config['base_url'] = current_url();
        $config['per_page'] = $this->limit;
        $config['total_rows'] = $this->News_Model->where('category_id', $category->id)->count_rows();
        $this->pagination->initialize($config);
        $vars['news_pagination'] = $this->pagination->create_links();

        $this->render('frontend/category/index', $vars);
    }
}
