<?php

class Zakat extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {}

    /**
     * @param integer $_GET['irrigation_cost_type']
     * @param integer $_GET['total']
     * @return object $response ['total' => '0']
     */
    public function maal_agriculture()
    {
        $irrigation_cost_type = $this->input->get('irrigation_cost_type');
        $total = (int) $this->input->get('total');
        $response['total'] = $this->zakat_library->maal_agriculture($total, $irrigation_cost_type);
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    /**
     * @param integer $_GET['code']
     * @param integer $_GET['owned']
     * @return object $response ['description' => '', total' => '0']
     */
    public function maal_farm_animals()
    {
        $code = $this->input->get('code');
        $owned = (int) $this->input->get('owned');
        $response = $this->zakat_library->maal_farm_animals($code, $owned);
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    /**
     * @param integer $_GET['owned']
     * @param integer $_GET['type']
     * @return object $response ['quantity' => '0']
     */
    public function maal_gold_and_silver()
    {
        $owned = (int) $this->input->get('owned');
        $type = $this->input->get('type');

        if ($type == 'gold') {
            $response['quantity'] = $this->zakat_library->maal_gold($owned);
        } else if ($type == 'silver') {
            $response['quantity'] = $this->zakat_library->maal_silver($owned);
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    /**
     * @param integer $_GET['nishab_total']
     * @param integer $_GET['total']
     * @return object ['total => '0']
     */
    public function maal_money()
    {
        $nishab_total = (int) $this->input->get('nishab_total');
        $total = (int) $this->input->get('total');
        $response['total'] = $this->zakat_library->maal_money($total, $nishab_total);
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    /**
     * @param integer $_GET['total']
     * @return object ['total => '0']
     */
    public function maal_rikaz()
    {
        $total = (int) $this->input->get('total');
        $response['total'] = $this->zakat_library->maal_rikaz($total);
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    /**
     * @param string $_GET['nishab_type'] 'gold' / 'silver'
     * @param integer $_GET['price']
     * @return object ['total_nishab' = '0']
     */
    public function get_nishab_total()
    {
        $nishab_type = $this->input->get('nishab_type');
        $price = $this->input->get('price');
        $response['nishab_total'] = $this->zakat_library->get_nishab_total($nishab_type, $price);
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    /**
     * @param integer $_GET['total_harvest']
     * @return object ['must_zakat' => '0']
     */
    public function get_must_zakat_maal_agriculture()
    {
        $total_harvest = $this->input->get('total_harvest');
        $response['must_zakat'] = $this->zakat_library->get_must_zakat_maal_agriculture($total_harvest);
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }
}
