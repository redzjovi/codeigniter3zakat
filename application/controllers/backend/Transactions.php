<?php

class Transactions extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Transaction_Detail_Model', 'Transactions_Model']);
    }

    public function index()
    {
        $this->input->get('status') ? $this->Transactions_Model->filter += ['status' => $this->input->get('status')] : '';
        $this->Transactions_Model->status = $this->input->get('status');

        $vars['transactions'] = $this->Transactions_Model->with_user()->where($this->Transactions_Model->filter)->order_by('created_at', 'DESC')->get_all();
        $vars['Transactions_Model'] = $this->Transactions_Model;
        $this->render('backend/transactions/index', $vars);
    }

    public function delete($id = 0)
    {
        $this->Transactions_Model->delete($id);
        $this->session->set_flashdata('message', lang('data_has_been_deleted'));
        redirect('backend/transactions');
    }

    public function view($id = 0)
    {
        $transaction = $this->Transactions_Model->with_user()->get($id) ?: show_404();

        $vars['Products_Model'] = $this->Products_Model;
        $vars['transaction'] = $transaction;
        $vars['transaction_details'] = $this->Transaction_Detail_Model->with_product()->where('transaction_id', $id)->get_all();
        $vars['Transactions_Model'] = $this->Transactions_Model;

        if ($this->input->post('update_status') && $this->Transactions_Model->validate('update_status')) {
            $this->Transactions_Model->update(['status' => $this->input->post('status')], $id);
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            redirect('backend/transactions/view/'.$id);
        }

        $this->render('backend/transactions/view', $vars);
    }
}
