<?php

class Answers extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Answers_Model', 'Questions_Model', 'Users_Model']);
    }

    public function index()
    {
        $vars['answers'] = $this->Answers_Model->with_question()->with_user()->order_by('created_at', 'DESC')->get_all();
        $this->render('backend/answers/index', $vars);
    }

    public function create()
    {
        $vars['question_id'] = $this->input->get('question_id');
        $vars['questions_options'] = $this->Questions_Model->get_questions_options();
        $vars['user_id'] = $this->input->get('user_id');
        $vars['user_options'] = $this->Users_Model->get_user_options();

        if ($this->input->post() && $this->Answers_Model->validate('create')) {
            $this->Answers_Model->create($this->input->post());
            $this->session->set_flashdata('message', lang('data_has_been_created'));

            $this->input->get('question_id') ? redirect('backend/questions/update/'.$this->input->get('question_id')) : redirect('backend/answers');
        }

        $this->render('backend/answers/form', $vars);
    }

    public function delete($id = 0)
    {
        $answer = $this->Answers_Model->get($id) ?: show_404();

        $this->Answers_Model->delete($id);
        $this->Questions_Model->total_answer_recalculate($answer->question_id);
        $this->session->set_flashdata('message', lang('data_has_been_deleted'));
        $this->agent->referrer() ? redirect($this->agent->referrer()) : redirect('backend/answers');
    }

    public function update($id = 0, $last_url = '')
    {
        $answer = $this->Answers_Model->get($id) ?: show_404();

        $vars['answer'] = $answer;
        $vars['questions_options'] = $this->Questions_Model->get_questions_options();
        $vars['user_options'] = $this->Users_Model->get_user_options();

        if ($this->input->post() && $this->Answers_Model->validate('update')) {
            $this->Answers_Model->update($this->input->post(), $this->input->post('id'));
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            $last_url ? redirect(base64_decode($last_url)) : redirect('backend/answers');
        }

        $this->render('backend/answers/form', $vars);
    }
}
