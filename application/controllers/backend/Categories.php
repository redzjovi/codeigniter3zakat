<?php

class Categories extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Categories_Model');
    }

    public function index()
    {
        $vars['categories'] = $this->Categories_Model->order_by('name')->get_all();
        $this->render('backend/categories/index', $vars);
    }

    public function create()
    {
        if ($this->input->post() && $this->Categories_Model->validate('create')) {
            $this->Categories_Model->create($this->input->post());
            $this->session->set_flashdata('message', lang('data_has_been_created'));
            redirect('backend/categories');
        }

        $this->render('backend/categories/form');
    }

    public function delete($id = 0)
    {
        $this->Categories_Model->delete($id);
        $this->session->set_flashdata('message', lang('data_has_been_deleted'));
        redirect('backend/categories');
    }

    public function update($id = 0)
    {
        $category = $this->Categories_Model->get($id) ?: show_404();

        $vars['category'] = $category;

        if ($this->input->post() && $this->Categories_Model->validate('update')) {
            $this->Categories_Model->update($this->input->post(), $this->input->post('id'));
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            redirect('backend/categories');
        }

        $this->render('backend/categories/form', $vars);
    }
}
