<?php

class News extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Categories_Model', 'News_Model']);
    }

    public function index()
    {
        $vars['news'] = $this->News_Model->with_category()->order_by('created_at', 'DESC')->get_all();
        $this->render('backend/news/index', $vars);
    }

    public function create()
    {
        $vars['categories_options'] = $this->Categories_Model->get_categories_options();

        if ($this->input->post() && $this->News_Model->validate('create')) {
            $this->News_Model->create($this->input->post());
            $this->session->set_flashdata('message', lang('data_has_been_created'));
            redirect('backend/news');
        }

        $this->render('backend/news/form', $vars);
    }

    public function delete($id = 0)
    {
        $this->News_Model->delete($id);
        $this->session->set_flashdata('message', lang('data_has_been_deleted'));
        redirect('backend/news');
    }

    public function update($id = 0)
    {
        $news = $this->News_Model->get($id) ?: show_404();

        $vars['categories_options'] = $this->Categories_Model->get_categories_options();
        $vars['news'] = $news;

        if ($this->input->post() && $this->News_Model->validate('update')) {
            $this->News_Model->update($this->input->post(), $this->input->post('id'));
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            redirect('backend/news');
        }

        $this->render('backend/news/form', $vars);
    }
}
