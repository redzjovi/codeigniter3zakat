<?php

class Questions extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Answers_Model', 'Questions_Model', 'Users_Model']);
    }

    public function index()
    {
        $vars['questions'] = $this->Questions_Model->with_user()->order_by('created_at', 'DESC')->get_all();
        $this->render('backend/questions/index', $vars);
    }

    public function create()
    {
        $vars['user_options'] = $this->Users_Model->get_user_options();

        if ($this->input->post() && $this->Questions_Model->validate('create')) {
            $this->Questions_Model->create($this->input->post());
            $this->session->set_flashdata('message', lang('data_has_been_created'));
            redirect('backend/questions');
        }

        $this->render('backend/questions/form', $vars);
    }

    public function delete($id = 0)
    {
        $this->Questions_Model->delete($id);
        $this->session->set_flashdata('message', lang('data_has_been_deleted'));
        redirect('backend/questions');
    }

    public function update($id = 0)
    {
        $question = $this->Questions_Model->get($id) ?: show_404();

        $vars['answers'] = $this->Answers_Model->with_user()->where('question_id', $id)->order_by('created_at')->get_all();
        $vars['user_options'] = $this->Users_Model->get_user_options();
        $vars['question'] = $question;

        if ($this->input->post() && $this->Questions_Model->validate('update')) {
            $this->Questions_Model->update($this->input->post(), $this->input->post('id'));
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            redirect('backend/questions');
        }

        $this->render('backend/questions/form', $vars);
    }
}
