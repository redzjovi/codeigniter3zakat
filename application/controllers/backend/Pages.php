<?php

class Pages extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pages_Model');
    }

    public function index()
    {
        $vars['pages'] = $this->Pages_Model->order_by('id', 'DESC')->get_all();
        $this->render('backend/pages/index', $vars);
    }

    public function create()
    {
        $vars['Pages_Model'] = $this->Pages_Model;

        if ($this->input->post() && $this->Pages_Model->validate('create')) {
            $this->Pages_Model->create($this->input->post());
            $this->session->set_flashdata('message', lang('data_has_been_created'));
            redirect('backend/pages');
        }

        $this->render('backend/pages/form', $vars);
    }

    public function delete($id = 0)
    {
        $this->Pages_Model->delete($id);
        $this->session->set_flashdata('message', lang('data_has_been_deleted'));
        redirect('backend/pages');
    }

    public function update($id = 0)
    {
        $page = $this->Pages_Model->get($id) ?: show_404();

        $vars['page'] = $page;
        $vars['Pages_Model'] = $this->Pages_Model;

        if ($this->input->post() && $this->Pages_Model->validate('update')) {
            $this->Pages_Model->update($this->input->post(), $this->input->post('id'));
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            redirect('backend/pages');
        }

        $this->render('backend/pages/form', $vars);
    }
}
