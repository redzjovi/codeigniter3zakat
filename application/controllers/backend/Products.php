<?php

class Products extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Products_Model');
    }

    public function index()
    {
        $vars['products'] = $this->Products_Model->order_by('name')->get_all();
        $this->render('backend/products/index', $vars);
    }

    public function create()
    {
        $vars['Products_Model'] = $this->Products_Model;

        if ($this->input->post() && $this->Products_Model->validate('create')) {
            $this->Products_Model->create($this->input->post());
            $this->session->set_flashdata('message', lang('data_has_been_created'));
            redirect('backend/products');
        }

        $this->render('backend/products/form', $vars);
    }

    public function delete($id = 0)
    {
        $this->Products_Model->delete($id);
        $this->session->set_flashdata('message', lang('data_has_been_deleted'));
        redirect('backend/products');
    }

    public function update($id = 0)
    {
        $product = $this->Products_Model->get($id) ?: show_404();

        $vars['product'] = $product;
        $vars['Products_Model'] = $this->Products_Model;

        if ($this->input->post() && $this->Products_Model->validate('update')) {
            $this->Products_Model->update($this->input->post(), $this->input->post('id'));
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            redirect('backend/products');
        }

        $this->render('backend/products/form', $vars);
    }
}
