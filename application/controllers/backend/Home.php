<?php

class Home extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Answers_Model', 'Questions_Model', 'Transactions_Model']);
    }

    public function index()
    {
        $vars['answers_count'] = $this->Answers_Model->count_rows();
        $vars['questions_count'] = $this->Questions_Model->count_rows();
        $vars['transactions_expired_count'] = $this->Transactions_Model->where('status', $this->Transactions_Model->status_expired)->count_rows();
        $vars['Transactions_Model'] = $this->Transactions_Model;
        $vars['transactions_paid_count'] = $this->Transactions_Model->where('status', $this->Transactions_Model->status_paid)->count_rows();
        $vars['transactions_unpaid_count'] = $this->Transactions_Model->where('status', $this->Transactions_Model->status_unpaid)->count_rows();
        $vars['transactions_waiting_count'] = $this->Transactions_Model->where('status', $this->Transactions_Model->status_waiting)->count_rows();
        $this->render('backend/home/index', $vars);
    }
}
