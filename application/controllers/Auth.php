<?php

class Auth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->layout = 'auth';
        $this->load->model('Users_Model');
    }

    public function forgot_password()
    {
        if ($this->input->post('forgot_password') && $this->Users_Model->validate('forgot_password')) {
            $vars['user'] = $this->Users_Model->where('email', $this->input->post('email'))->get();

            $config = $this->email->get_config();
            $this->email->initialize($config);
            $this->email->from($config['smtp_user'], $config['smtp_user']);
            $this->email->to($this->input->post('email'));
            $this->email->subject(lang('reset_password'));
            $this->email->message($this->load->view('frontend/auth/_reset_password_email', $vars, true));
            ($this->email->send()) ?: show_error($this->email->print_debugger());

            $this->session->set_flashdata('message', lang('please_check_your_email_and_follow_the_instruction_to_reset_your_password'));
            redirect('auth/sign_in');
        }

        $this->render('frontend/auth/forgot_password');
    }

    public function reset_password()
    {
        $user = $this->Users_Model->where(['email' => $this->input->get('email'), 'password' => $this->input->get('password')])->get() ?: show_404();

        if ($this->input->post('reset_password') && $this->Users_Model->validate('reset_password')) {
            $this->Users_Model->update($this->input->post(), ['email' => $this->input->get('email')]);
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            redirect('auth/sign_in');
        }

        $this->render('frontend/auth/reset_password');
    }

    public function sign_in()
    {
        if ($this->input->post() && $this->Users_Model->validate('sign_in')) {
            $user = $this->Users_Model->where(['email' => $this->input->post('email')])->get();
            $this->session->set_userdata('user', $user);

            $last_url = base64_decode($this->input->get('last_url'));
            $last_url ? redirect($last_url) : redirect();
        }
        $this->render('frontend/auth/sign_in');
    }

    public function sign_out()
    {
        $this->session->unset_userdata('user');
        redirect();
    }

    public function sign_up()
    {
        $_POST['user_type'] = $this->Users_Model->user_type_user;
        if ($this->input->post() && $this->Users_Model->validate('sign_up')) {
            $id = $this->Users_Model->create($this->input->post());
            $user = $this->Users_Model->get($id);
            $this->session->set_userdata('user', $user);
            redirect();
        }
        $this->render('frontend/auth/sign_up');
    }
}
