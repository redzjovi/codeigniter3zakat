<?php

class Transactions extends Frontend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Pages_Model', 'Products_Model', 'Transaction_Detail_Model', 'Transactions_Model']);
    }

    public function index()
    {
        $this->is_login() ?: redirect('auth/sign_in?last_url='.base64_encode(current_url_with_params()));
        $vars['transactions'] = $this->Transactions_Model->with_user()->where('user_id', $this->user->id)->order_by('created_at', 'DESC')->get_all();
        $vars['Transactions_Model'] = $this->Transactions_Model;
        $this->render('frontend/transactions/index', $vars);
    }

    public function detail($id)
    {
        $this->is_login() ?: redirect('auth/sign_in?last_url='.base64_encode(current_url_with_params()));
        $transaction = $this->Transactions_Model->with_user()->get($id) ?: show_404();

        $vars['Products_Model'] = $this->Products_Model;
        $vars['transaction'] = $transaction;
        $vars['transaction_details'] = $this->Transaction_Detail_Model->with_product()->where('transaction_id', $id)->get_all();
        $vars['Transactions_Model'] = $this->Transactions_Model;

        if ($this->input->post() && $this->Transactions_Model->validate('submit_receipt')) {
            $_POST['status'] = $this->Transactions_Model->status_waiting;
            $this->Transactions_Model->update($this->input->post(), $this->input->post('id'));
            $this->session->set_flashdata('message', lang('data_has_been_updated'));
            redirect('transaction/detail/'.$id);
        }

        $this->render('frontend/transactions/detail', $vars);
    }
}
