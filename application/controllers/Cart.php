<?php

class Cart extends Frontend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Pages_Model', 'Products_Model', 'Transaction_Detail_Model', 'Transactions_Model']);
    }

    public function index()
    {
        $vars['contents'] = $this->cart->contents();
        $vars['Products_Model'] = $this->Products_Model;

        $this->render('frontend/cart/index', $vars);
    }

    public function checkout()
    {
        $this->is_login() ?: redirect('auth/sign_in?last_url='.base64_encode(site_url('cart')));

        if ($contents = $this->cart->contents()) {
            $transaction_id = $this->Transactions_Model->create(['user_id' => $this->user->id, 'total' => $this->cart->total(), 'status' => $this->Transactions_Model->status_unpaid]);
            foreach ($contents as $i => $content) {
                $this->Transaction_Detail_Model->create(['transaction_id' => $transaction_id, 'product_id' => $content['options']['product_id'], 'quantity' => $content['qty'], 'price' => $content['price'], 'sub_total' => $content['subtotal'], 'options' => json_encode($content['options'])]);
            }
            $this->cart->destroy();
            $this->session->set_flashdata('message', lang('transaction_has_been_created'));

            $config = $this->email->get_config();
            $this->email->initialize($config);
            $this->email->from($config['smtp_user'], $config['smtp_user']);
            $this->email->to($this->user->email);
            $this->email->subject(lang('how_to_pay'));
            $this->email->message($this->load->view('frontend/cart/_how_to_pay', '', true));
            ($this->email->send()) ?: show_error($this->email->print_debugger());

            redirect('transaction/detail/'.$transaction_id);
        }

        redirect('cart');
    }

    public function delete($id)
    {
        $this->cart->remove($id);
        redirect('cart');
    }
}
