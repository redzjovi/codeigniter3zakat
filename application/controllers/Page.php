<?php

class Page extends Frontend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pages_Model');
    }

    public function index()
    {
        show_404();
    }

    public function detail($slug = '')
    {
        ($page = $this->Pages_Model->where('slug', $slug)->get()) ?: show_404();

        $vars = $this->detail_vars($page->template);
        $vars['page'] = $page;
        // dump($vars);
        // dump($this->cart->contents());

        $this->detail_post($page->template);
        $this->render('frontend/page/detail/'.$page->template, $vars);
    }

    public function detail_post($template)
    {
        switch ($template) {
            case 'contact_us' :
                $this->contact_us_post(); break;
            case 'zakat_fitrah' :
                $this->zakat_fitrah_post(); break;
            case 'zakat_maal_agriculture' :
                $this->zakat_maal_agriculture_post(); break;
            case 'zakat_maal_farm_animals' :
                $this->zakat_maal_farm_animals_post(); break;
            case 'zakat_maal_gold_and_silver' :
                $this->zakat_maal_gold_and_silver_post(); break;
            case 'zakat_maal_money' :
                $this->zakat_maal_money_post(); break;
            case 'zakat_maal_rikaz' :
                $this->zakat_maal_rikaz_post(); break;
            case 'zakat_maal_trading' :
                $this->zakat_maal_trading_post(); break;
        }
    }

    public function detail_vars($template)
    {
        $vars = [];

        switch ($template) {
            case 'zakat_fitrah' :
                $vars = $this->zakat_fitrah_vars(); break;
            case 'zakat_maal_agriculture' :
                $vars = $this->zakat_maal_agriculture_vars(); break;
            case 'zakat_maal_farm_animals' :
                $vars = $this->zakat_maal_farm_animals_vars(); break;
            case 'zakat_maal_gold_and_silver' :
                $vars = $this->zakat_maal_gold_and_silver_vars(); break;
            case 'zakat_maal_money' :
                $vars = $this->zakat_maal_money_vars(); break;
            case 'zakat_maal_rikaz' :
                $vars = $this->zakat_maal_rikaz_vars(); break;
            case 'zakat_maal_trading' :
                $vars = $this->zakat_maal_trading_vars(); break;
        }

        return $vars;
    }

    // page detail
    public function contact_us_post()
    {
        $this->load->model('form/Contact_Us');

        if ($this->input->post('send') && $this->Contact_Us->validate('contact_us')) {
            $config = $this->email->get_config();

            $this->email->initialize($config);
            $this->email->from($this->input->post('email'), $this->input->post('name'));
            $this->email->to($config['smtp_user']);
            $this->email->cc($this->input->post('email'));
            $this->email->subject(lang('contact_us'));
            $this->email->message($this->input->post('message'));
            ($this->email->send()) ?: show_error($this->email->print_debugger());

            $this->session->set_flashdata('message', lang('message_has_been_sent'));
            redirect('page/hubungi-kami', 'refresh');
        }
    }

    public function zakat_fitrah_post()
    {
        $this->load->model(['Products_Model', 'Transaction_Detail_Model']);

        if ($this->input->post('add_to_cart') && $this->Transaction_Detail_Model->validate('add_to_cart')) {
            foreach ($this->input->post('product_id') as $i => $product) {
                $data = [
                    'id' => time(),
                    'qty' => $this->input->post('quantity['.$i.']'),
                    'price' => $this->input->post('price['.$i.']'),
                    'name' => $this->input->post('product_name['.$i.']'),
                    'options' => [
                        'product_id' => $this->input->post('product_id['.$i.']'),
                        'product_name' => $this->input->post('product_name['.$i.']'),
                        'unit' => 'L',
                    ],
                ];
                $this->cart->product_name_safe = false;
                $this->cart->insert($data);
            }
            $this->session->set_flashdata('message', lang('zakat_has_been_added_to_cart'));
            redirect(current_url_with_params(), 'refresh');
        }
    }
    public function zakat_fitrah_vars()
    {
        $this->load->model('Products_Model');

        $vars['products'] = $this->Products_Model->where('type', 'zakat_fitrah')->get_all();
        $vars['Products_Model'] = $this->Products_Model;
        return $vars;
    }

    public function zakat_maal_agriculture_post()
    {
        $this->load->model('Transaction_Detail_Model');

        if ($this->input->post('add_to_cart') && $this->Transaction_Detail_Model->validate('add_to_cart_zakat_maal_agriculture')) {
            $data = [
                'id' => time(),
                'qty' => $this->input->post('total'),
                'price' => 0,
                'name' => $this->input->post('product_name'),
                'options' => [
                    'product_id' => $this->input->post('product_id'),
                    'product_name' => $this->input->post('product_name'),
                    'unit' => 'kg',

                    'harvest' => $this->input->post('harvest'),
                    'must_zakat' => $this->input->post('must_zakat'),
                    'irrigation_cost_type' => $this->input->post('irrigation_cost_type'),
                    'total' => $this->input->post('total'),
                ],
            ];
            $this->cart->product_name_safe = false;
            $this->cart->insert($data);
            $this->session->set_flashdata('message', lang('zakat_has_been_added_to_cart'));
            redirect(current_url_with_params(), 'refresh');
        }
    }
    public function zakat_maal_agriculture_vars()
    {
        $this->load->model('Products_Model');

        $vars['product'] = $this->Products_Model->where('type', 'zakat_maal_agriculture')->get();
        $vars['Products_Model'] = $this->Products_Model;
        return $vars;
    }

    public function zakat_maal_farm_animals_post()
    {
        $this->load->model('Transaction_Detail_Model');

        if ($this->input->post('add_to_cart') && $this->Transaction_Detail_Model->validate('add_to_cart_zakat_maal_farm_animals')) {
            foreach ($this->input->post('product_id') as $i => $product) {
                if ($this->input->post('quantity['.$i.']') <= 0) { continue; }
                $data = [
                    'id' => time(),
                    'qty' => $this->input->post('quantity['.$i.']'),
                    'price' => 0,
                    'name' => $this->input->post('product_name['.$i.']'),
                    'options' => [
                        'product_id' => $this->input->post('product_id['.$i.']'),
                        'product_name' => $this->input->post('product_name['.$i.']'),
                        'unit' => $this->input->post('description['.$i.']'),

                        'animals_owned' => $this->input->post('animals_owned['.$i.']'),
                        'description' => $this->input->post('description['.$i.']'),
                    ],
                ];
                $this->cart->product_name_safe = false;
                $this->cart->insert($data);
            }
            $this->session->set_flashdata('message', lang('zakat_has_been_added_to_cart'));
            redirect(current_url_with_params(), 'refresh');
        }
    }
    public function zakat_maal_farm_animals_vars()
    {
        $this->load->model('Products_Model');

        $vars['products'] = $this->Products_Model->where('type', 'zakat_maal_farm_animals')->order_by('name')->get_all();
        $vars['Products_Model'] = $this->Products_Model;
        return $vars;
    }

    public function zakat_maal_gold_and_silver_post()
    {
        $this->load->model('Transaction_Detail_Model');

        if ($this->input->post('add_to_cart') && $this->Transaction_Detail_Model->validate('add_to_cart_zakat_maal_gold_and_silver')) {
            foreach ($this->input->post('product_id') as $i => $product) {
                if ($this->input->post('quantity['.$i.']') <= 0) { continue; }
                $data = [
                    'id' => time(),
                    'qty' => $this->input->post('quantity['.$i.']'),
                    'price' => $this->input->post('price['.$i.']'),
                    'name' => $this->input->post('product_name['.$i.']'),
                    'options' => [
                        'product_id' => $this->input->post('product_id['.$i.']'),
                        'product_name' => $this->input->post('product_name['.$i.']'),
                        'unit' => 'gr',
                    ],
                ];
                $this->cart->product_name_safe = false;
                $this->cart->insert($data);
            }
            $this->session->set_flashdata('message', lang('zakat_has_been_added_to_cart'));
            redirect(current_url_with_params(), 'refresh');
        }
    }
    public function zakat_maal_gold_and_silver_vars()
    {
        $this->load->model('Products_Model');

        $vars['products'] = $this->Products_Model->where('type', 'zakat_maal_gold_and_silver')->order_by('name')->get_all();
        $vars['Products_Model'] = $this->Products_Model;
        return $vars;
    }

    public function zakat_maal_money_post()
    {
        $this->load->model(['Products_Model', 'Transaction_Detail_Model']);

        if ($this->input->post('add_to_cart') && $this->Transaction_Detail_Model->validate('add_to_cart_zakat_maal_money')) {
            $data = [
                'id' => time(),
                'qty' => 1,
                'price' => $this->input->post('total'),
                'name' => $this->input->post('product_name'),
                'options' => [
                    'product_id' => $this->input->post('product_id'),
                    'product_name' => $this->input->post('product_name'),
                    'unit' => '',

                    'nishab_type' => $this->input->post('nishab_type'),
                    'nishab_total' => $this->input->post('nishab_total'),
                    'cash_and_deposit' => $this->input->post('cash_and_deposit'),
                    'stock_and_other_valuable' => $this->input->post('stock_and_other_valuable'),
                    'accounts_receivable' => $this->input->post('accounts_receivable'),
                    'assets_total' => $this->input->post('assets_total'),
                    'debit' => $this->input->post('debit'),
                    'obligations_total' => $this->input->post('obligations_total'),
                    'differences_between_assets_and_obligations' => $this->input->post('differences_between_assets_and_obligations'),
                    'total' => $this->input->post('total'),
                ],
            ];
            $this->cart->product_name_safe = false;
            $this->cart->insert($data);
            $this->session->set_flashdata('message', lang('zakat_has_been_added_to_cart'));
            redirect(current_url_with_params(), 'refresh');
        }
    }
    public function zakat_maal_money_vars()
    {
        $this->load->model('Products_Model');

        $vars['product'] = $this->Products_Model->where('type', 'zakat_maal_money')->get();
        $vars['products'] = $this->Products_Model->where('type', 'zakat_maal_gold_and_silver')->order_by('name')->get_all();
        $vars['Products_Model'] = $this->Products_Model;
        $vars['zakat_maal_money'] = $this->Products_Model->where('type', 'zakat_maal_money')->get();
        return $vars;
    }

    public function zakat_maal_rikaz_post()
    {
        $this->load->model('Transaction_Detail_Model');

        if ($this->input->post('add_to_cart') && $this->Transaction_Detail_Model->validate('add_to_cart_zakat_maal_rikaz')) {
            $data = [
                'id' => time(),
                'qty' => 1,
                'price' => $this->input->post('total'),
                'name' => $this->input->post('product_name'),
                'options' => [
                    'product_id' => $this->input->post('product_id'),
                    'product_name' => $this->input->post('product_name'),
                    'unit' => $this->Products_Model->currency,

                    'treasure_value' => $this->input->post('treasure_value'),
                    'total' => $this->input->post('total'),
                ],
            ];
            $this->cart->product_name_safe = false;
            $this->cart->insert($data);
            $this->session->set_flashdata('message', lang('zakat_has_been_added_to_cart'));
            redirect(current_url_with_params(), 'refresh');
        }
    }
    public function zakat_maal_rikaz_vars()
    {
        $this->load->model('Products_Model');

        $vars['product'] = $this->Products_Model->where('type', 'zakat_maal_rikaz')->get();
        $vars['Products_Model'] = $this->Products_Model;
        return $vars;
    }

    public function zakat_maal_trading_post()
    {
        $this->load->model('Transaction_Detail_Model');

        if ($this->input->post('add_to_cart') && $this->Transaction_Detail_Model->validate('add_to_cart_zakat_maal_money')) {
            $data = [
                'id' => time(),
                'qty' => 1,
                'price' => $this->input->post('total'),
                'name' => $this->input->post('product_name'),
                'options' => [
                    'product_id' => $this->input->post('product_id'),
                    'product_name' => $this->input->post('product_name'),
                    'unit' => '',

                    'nishab_type' => $this->input->post('nishab_type'),
                    'nishab_total' => $this->input->post('nishab_total'),
                    'cash_and_deposit' => $this->input->post('cash_and_deposit'),
                    'stock_of_items' => $this->input->post('stock_of_items'),
                    'accounts_receivable' => $this->input->post('accounts_receivable'),
                    'assets_total' => $this->input->post('assets_total'),
                    'debit' => $this->input->post('debit'),
                    'obligations_total' => $this->input->post('obligations_total'),
                    'differences_between_assets_and_obligations' => $this->input->post('differences_between_assets_and_obligations'),
                    'total' => $this->input->post('total'),
                ],
            ];
            $this->cart->product_name_safe = false;
            $this->cart->insert($data);
            $this->session->set_flashdata('message', lang('zakat_has_been_added_to_cart'));
            redirect(current_url_with_params(), 'refresh');
        }
    }
    public function zakat_maal_trading_vars()
    {
        $this->load->model('Products_Model');

        $vars['product'] = $this->Products_Model->where('type', 'zakat_maal_trading')->get();
        $vars['products'] = $this->Products_Model->where('type', 'zakat_maal_gold_and_silver')->order_by('name')->get_all();
        $vars['Products_Model'] = $this->Products_Model;
        $vars['zakat_maal_money'] = $this->Products_Model->where('type', 'zakat_maal_money')->get();
        return $vars;
    }
}
