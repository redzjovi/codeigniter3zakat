<?php

class Pages_Model extends MY_Model
{
    public $table = 'pages';
    public $primary_key = 'id';
    public $delete_cache_on_save = true;

    public function __construct()
    {
        parent::__construct();
        $this->after_get = ['get_image_file_info'];
        $this->before_delete = ['delete_files'];
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'create' :
                $this->form_validation->set_rules('category_id', lang('category'), ['trim', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('template', lang('template'), ['trim', 'max_length[255]']);
                break;
            case 'update' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('category_id', lang('category'), ['trim', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('template', lang('template'), ['trim', 'max_length[255]']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('slug', lang('slug'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim']);
                $this->form_validation->set_rules('image', lang('name'), ['trim', 'max_length[255]']);

                $this->form_validation->set_rules('template', lang('template'), ['trim', 'max_length[255]']);
                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);
                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $data
     * [
     *      'title' => 'title',
     *      'content' => 'content',
     *      'image' => 'image',
     *      'template' => 'default',
     * ]
     * @return integer $id
     */
    public function create($data = [])
    {
        if (isset($data['title'])) { $data['slug'] = url_title($data['title'], '-', true); }

        $data['id'] = $id = $this->insert($data);
        $this->update($data, $id);

        return $id;
    }

    public function delete_files($data)
    {
        $path = $this->upload->pages_path.'/'.$data['id'];
        delete_files($path, true); // file_helper
        is_dir($path) ? rmdir($path) : '';
    }

    public function file_upload($temp_file = '', $new_path = '')
    {
        $new_file = '';

        if (file_exists($temp_file)) {
            if (! is_dir($new_path)) { mkdir($new_path, 0755, true); }
            $temp_file_info = get_file_info($temp_file);
            $new_file = $new_path.'/'.$temp_file_info['name'];
            rename($temp_file, $new_file);
        }

        foreach (glob($new_path.'/*') as $old_file) {
            if (! in_array(basename($old_file), [basename($new_file)])) { unlink($old_file); }
        }

        return $new_file;
    }

    public function get_image_file_info($data)
    {
        if ($this->is_assoc($data)) {
            $file_info = get_file_info($data['image']);
            $data['image_name'] = $file_info['name'];
            $data['image_size'] = $file_info['size'];
        }
        return $data;
    }

    public function get_template_options()
    {
        $options = ['default' => lang('default')];
        $template = [
            'footer' => lang('footer'),
            'home' => lang('home'),
            'how_to_pay' => lang('how_to_pay'),
            'transaction_status_has_been_changed' => lang('transaction_status_has_been_changed'),
            'zakat_fitrah' => lang('zakat_fitrah'),
            'zakat_maal_agriculture' => lang('zakat_maal_agriculture'),
            'zakat_maal_farm_animals' => lang('zakat_maal_farm_animals'),
            'zakat_maal_gold_and_silver' => lang('zakat_maal_gold_and_silver'),
            'zakat_maal_money' => lang('zakat_maal_money'),
            'zakat_maal_rikaz' => lang('zakat_maal_rikaz'),
            'zakat_maal_trading' => lang('zakat_maal_trading'),
        ];
        asort($template); $options += $template;
        return $options;
    }

    /**
     * @param array $data
     * [
     *      'id' => '1',
     *      'title' => 'title',
     *      'content' => 'content',
     *      'image' => 'image',
     *      'template' => 'default',
     * ]
     */
    public function update($data = [], $column_name_where = null, $escape = true)
    {
        if (isset($data['title'])) { $data['slug'] = url_title($data['title'], '-', true); }
        if (isset($data['image'])) { $data['image'] = $this->file_upload($data['image'], $this->upload->pages_path.'/'.$data['id']); }

        parent::update($data, $column_name_where);
    }
}
