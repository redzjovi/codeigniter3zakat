<?php

class Transaction_Detail_Model extends MY_Model
{
    public $table = 'transaction_detail';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->has_one['product'] = ['Products_Model', 'id', 'product_id'];
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'add_to_cart' :
                $this->form_validation->set_rules('product_id[]', lang('product'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('quantity[]', lang('quantity'), ['trim', 'required', 'numeric', 'greater_than[0]', 'max_length[20]']);
                $this->form_validation->set_rules('price[]', lang('price'), ['trim', 'required', 'integer', 'max_length[20]']);
                $this->form_validation->set_rules('sub_total[]', lang('sub_total'), ['trim', 'required', 'numeric', 'greater_than[0]', 'max_length[20]']);
                break;
            case 'add_to_cart_zakat_maal_agriculture' :
                $this->form_validation->set_rules('product_id', lang('product'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('total', lang('total'), ['trim', 'required', 'numeric', 'greater_than[0]', 'max_length[20]']);
                break;
            case 'add_to_cart_zakat_maal_farm_animals' :
                $this->form_validation->set_rules('product_id[]', lang('product'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('total', lang('total'), ['trim', 'required', 'numeric', 'greater_than[0]', 'max_length[20]']);
                break;
            case 'add_to_cart_zakat_maal_gold_and_silver' :
                $this->form_validation->set_rules('product_id[]', lang('product'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('quantity[]', lang('quantity'), ['trim', 'required', 'numeric', 'max_length[20]']);
                $this->form_validation->set_rules('price[]', lang('price'), ['trim', 'required', 'numeric', 'max_length[20]']);
                $this->form_validation->set_rules('total', lang('total'), ['trim', 'required', 'numeric', 'greater_than[0]', 'max_length[20]']);
                break;
            case 'add_to_cart_zakat_maal_money' :
                $this->form_validation->set_rules('product_id', lang('product'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('total', lang('total'), ['trim', 'required', 'numeric', 'greater_than[0]', 'max_length[20]']);
                break;
            case 'add_to_cart_zakat_maal_rikaz' :
                $this->form_validation->set_rules('product_id', lang('product'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('total', lang('total'), ['trim', 'required', 'numeric', 'greater_than[0]', 'max_length[20]']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('transaction_id', lang('transaction'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('product_id', lang('product'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('quantity', lang('quantity'), ['trim', 'required', 'numeric', 'max_length[20]']);
                $this->form_validation->set_rules('price', lang('price'), ['trim', 'required', 'numeric', 'max_length[20]']);

                $this->form_validation->set_rules('sub_total', lang('sub_total'), ['trim', 'required', 'numeric', 'max_length[20]']);
                $this->form_validation->set_rules('options', lang('options'), ['trim']);
                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);
                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $data
     * [
     *      'transaction_id' => '1',
     *      'product_id' => '1',
     *      'quantity' => '1',
     *      'price' => '1',
     *      'sub_total' => '1',
     *      'options' => json_encode([]),
     * ]
     * @return integer $id
     */
    public function create($data = [])
    {
        $id = parent::insert($data);
        return $id;
    }
}
