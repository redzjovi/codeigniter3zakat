<?php

class Products_Model extends MY_Model
{
    public $table = 'products';
    public $primary_key = 'id';

    public $currency = 'Rp.';
    public $return_type = 'object';

    public function __construct()
    {
        parent::__construct();
        $this->after_get = ['get_image_file_info'];
        $this->before_delete = ['delete_files'];
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'create' :
                $this->form_validation->set_rules('type', lang('type'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('code', lang('code'), ['trim', 'max_length[100]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[100]']);
                $this->form_validation->set_rules('price', lang('price'), ['trim', 'required', 'numeric', 'max_length[20]']);
                break;
            case 'update' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('type', lang('type'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('code', lang('code'), ['trim', 'max_length[100]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[100]']);
                $this->form_validation->set_rules('price', lang('price'), ['trim', 'required', 'numeric', 'max_length[20]']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('type', lang('type'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('code', lang('code'), ['trim', 'required', 'max_length[100]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[100]']);
                $this->form_validation->set_rules('slug', lang('slug'), ['trim', 'required', 'max_length[255]']);

                $this->form_validation->set_rules('price', lang('price'), ['trim', 'required', 'numeric', 'max_length[20]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim']);
                $this->form_validation->set_rules('images', lang('images'), ['trim']);
                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);
                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $data
     * [
     *      'type' => 'zakat_fitrah' / 'zakat_maal_agriculture' / 'zakat_maal_farm_animals' / 'zakat_maal_gold_and_silver' / 'zakat_maal_money' / 'zakat_maal_rikaz' / 'zakat_maal_trading',
     *      'code' => 'code',
     *      'name' => 'name',
     *      'price' => '1',
     *      'content' => 'content,'
     *      'images' => ['images'],
     * ]
     * @return integer $id
     */
    public function create($data = [])
    {
        if (isset($data['name'])) { $data['slug'] = url_title($data['name'], '-', true); }

        $data_insert = $data;
        unset($data_insert['images']);
        $data['id'] = $id = $this->insert($data_insert);
        $this->update($data, $id);

        return $id;
    }

    public function delete_files($data)
    {
        $path = $this->upload->products_path.'/'.$data['id'];
        delete_files($path, true); // file_helper
        is_dir($path) ? rmdir($path) : '';
    }

    public function file_upload($temp_files = [], $new_path = '')
    {
        $temp_files = array_filter($temp_files); // to remove empty value
        $new_files = $new_files_full_path = [];

        foreach ($temp_files as $temp_file) {
            if (file_exists($temp_file)) {
                if (! is_dir($new_path)) { mkdir($new_path, 0755, true); }
                $temp_file_info = get_file_info($temp_file);
                $new_file = $new_path.'/'.$temp_file_info['name'];
                rename($temp_file, $new_file);

                $new_files[] = $temp_file_info['name'];
                $new_files_full_path[] = $new_file;
            }
        }


        foreach (glob($new_path.'/*') as $old_file) {
            if (! in_array(basename($old_file), $new_files)) { unlink($old_file); }
        }

        return json_encode($new_files_full_path);
    }

    public function get_image_file_info($data)
    {
        if ($this->is_assoc($data)) {
            $images = json_decode($data['images']);
            $data['images'] = [];

            if ($images) {
                foreach ($images as $i => $image) {
                    $file_info = get_file_info($image);
                    $data['images'][$i]['image'] = $image;
                    $data['images'][$i]['image_name'] = $file_info['name'];
                    $data['images'][$i]['image_size'] = $file_info['size'];
                }
            }
        }
        return $data;
    }

    public function get_type_options()
    {
        $type_options = [
            '' => '- '.lang('choose_type').' -',
            'zakat_fitrah' => lang('zakat_fitrah'),
            'zakat_maal_agriculture' => lang('zakat_maal_agriculture'),
            'zakat_maal_farm_animals' => lang('zakat_maal_farm_animals'),
            'zakat_maal_gold_and_silver' => lang('zakat_maal_gold_and_silver'),
            'zakat_maal_money' => lang('zakat_maal_money'),
            'zakat_maal_rikaz' => lang('zakat_maal_rikaz'),
            'zakat_maal_trading' => lang('zakat_maal_trading'),
        ];
        asort($type_options);
        return $type_options;
    }

    /**
     * @param array $data
     * [
     *      'type' => 'zakat_fitrah' / 'zakat_maal_agriculture' / 'zakat_maal_farm_animals' / 'zakat_maal_gold_and_silver' / 'zakat_maal_money' / 'zakat_maal_rikaz' / 'zakat_maal_trading',
     *      'code' => 'code',
     *      'name' => 'name',
     *      'price' => '1',
     *      'images' => ['images'],
     * ]
     */
    public function update($data = [], $column_name_where = null, $escape = true)
    {
        if (isset($data['name'])) { $data['slug'] = url_title($data['name'].' '.$data['id'], '-', true); }
        if (isset($data['images'])) { $data['images'] = $this->file_upload($data['images'], $this->upload->products_path.'/'.$data['id']); }

        parent::update($data, $column_name_where);
    }
}
