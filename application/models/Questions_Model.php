<?php

class Questions_Model extends MY_Model
{
    public $table = 'questions';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->has_one['user'] = ['Users_Model', 'id', 'user_id'];
        $this->before_delete = ['delete_answers'];
        $this->load->model('Answers_Model');
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'create' :
                $this->form_validation->set_rules('user_id', lang('user'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim', 'required']);
                break;
            case 'update' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('user_id', lang('user'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim', 'required']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('user_id', lang('user'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('slug', lang('slug'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim', 'required']);

                $this->form_validation->set_rules('total_answer', lang('total_answer'), ['trim', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);
                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $data
     * [
     *      'user_id' => '1',
     *      'title' => 'title',
     *      'content' => 'content',
     * ]
     * @return integer $id
     */
    public function create($data = [])
    {
        if (isset($data['title'])) { $data['slug'] = url_title($data['title'], '-', true); }
        $data['total_answer'] = 0;

        $data['id'] = $id = $this->insert($data);
        $data['slug'] = url_title($data['title'].' '.$id, '-', true);
        $this->update($data, $id);

        return $id;
    }

    public function delete_answers($data)
    {
        $this->Answers_Model->delete($data['id']);
    }

    public function get_questions_options()
    {
        $options = ['' => '- '.lang('choose_question').' -'];
        $questions = $this->order_by('title')->as_array()->get_all();
        ! count($questions) ?: $options += array_column($questions, 'title', 'id');
        return $options;
    }

    /**
     * @param integer $id
     */
    public function total_answer_recalculate($id = 0)
    {
        $total_answer = $this->Answers_Model->where('question_id', $id)->count_rows();
        $this->update(['total_answer' => $total_answer], $id);
    }

    /**
     * @param array $data
     * [
     *      'id' => '1',
     *      'user_id' => '1',
     *      'title' => 'title',
     *      'content' => 'content',
     * ]
     */
    public function update($data = [], $column_name_where = null, $escape = true)
    {
        if (isset($data['title'])) { $data['slug'] = url_title($data['title'].' '.$data['id'], '-', true); }

        parent::update($data, $column_name_where);
    }
}
