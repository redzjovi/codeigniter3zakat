<?php

class Transactions_Model extends MY_Model
{
    public $table = 'transactions';
    public $primary_key = 'id';

    public $filter = [];
    public $status;
    public $status_expired = 'expired';
    public $status_paid = 'paid';
    public $status_unpaid = 'unpaid';
    public $status_waiting = 'waiting';

    public function __construct()
    {
        parent::__construct();
        $this->has_one['user'] = ['Users_Model', 'id', 'user_id'];
        $this->after_get = ['get_upload_file_info'];
        $this->before_delete = ['delete_transaction_detail'];
        $this->load->model('Transaction_Detail_Model');
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'submit_receipt' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('upload', lang('upload'), ['trim', 'required']);
                break;
            case 'update_status' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('status', lang('status'), ['trim', 'required']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('user_id', lang('user'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('total', lang('total'), ['trim', 'required', 'numeric', 'max_length[20]']);
                $this->form_validation->set_rules('upload', lang('upload'), ['trim', 'required']);
                $this->form_validation->set_rules('status', lang('status'), ['trim', 'required']);

                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);
                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $data
     * [
     *      'user_id' => '1',
     *      'total' => 'total',
     *      'status' => 'paid' / 'unpaid',
     * ]
     * @return integer $id
     */
    public function create($data = [])
    {
        $id = parent::insert($data);
        return $id;
    }

    public function delete_transaction_detail($data)
    {
        $id = $data['id'];
        $this->Transaction_Detail_Model->delete(['transaction_id' => $id]);
    }

    public function get_status_color_class($status)
    {
        $color_class = '';
        if ($status == $this->status_unpaid) { $color_class = 'btn-primary'; }
        else if ($status == $this->status_waiting) { $color_class = 'btn-warning'; }
        else if ($status == $this->status_paid) { $color_class = 'btn-success'; }
        else if ($status == $this->status_expired) { $color_class = 'btn-danger'; }
        return $color_class;
    }

    public function get_status_options()
    {
        $options = [
            $this->status_expired => lang('expired'),
            $this->status_waiting => lang('waiting'),
            $this->status_paid => lang('paid'),
            $this->status_unpaid => lang('unpaid'),
        ];
        return $options;
    }

    public function get_upload_file_info($data)
    {
        if ($this->is_assoc($data)) {
            $file_info = get_file_info($data['upload']);
            $data['upload_name'] = $file_info['name'];
            $data['upload_size'] = $file_info['size'];
        }
        return $data;
    }

    /**
     * @param array $data
     * [
     *      'id' => '1',
     *      'user_id' => '1',
     *      'total' => '1',
     *      'upload' => 'file_name',
     *      'status' => get_status_options(),
     * ]
     */
    public function update($data = [], $column_name_where = null, $escape = true)
    {
        if (isset($data['upload'])) { $data['upload'] = $this->upload->file_upload($data['upload'], $this->upload->transactions_path.'/'.$data['id']); }

        $this->status_changed($data, $column_name_where);

        parent::update($data, $column_name_where);
    }

    public function status_changed($data, $column_name_where)
    {
        if (isset($data['status'])) {
            $transaction = $this->with_user()->where($column_name_where)->get();

            if ($transaction->status != $data['status']) {
                $vars['data'] = $data;
                $vars['transaction'] = $transaction;

                $config = $this->email->get_config();
                $this->email->initialize($config);
                $this->email->from($config['smtp_user'], $config['smtp_user']);
                $this->email->to($transaction->user->email);
                $this->email->bcc($config['smtp_user']);
                $this->email->subject(lang('transaction_status_has_been_changed'));
                $this->email->message($this->load->view('backend/transactions/_transaction_status_has_been_changed', $vars, true));
                ($this->email->send()) ?: show_error($this->email->print_debugger());
            }
        }
    }
}
