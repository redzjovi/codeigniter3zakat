<?php

class Categories_Model extends MY_Model
{
    public $table = 'categories';
    public $primary_key = 'id';
    public $delete_cache_on_save = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'create' :
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[255]']);
                break;
            case 'update' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[255]']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('slug', lang('slug'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);
                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $data
     * [
     *      'name' => 'name',
     * ]
     * return integer $id
     */
    public function create($data = [])
    {
        if (isset($data['name'])) { $data['slug'] = url_title($data['name'], '-', true); }

        $data['id'] = $id = $this->insert($data);
        $this->update($data, $id);

        return $id;
    }

    public function get_categories_options()
    {
        $options = ['' => '- '.lang('choose_category').' -'];
        $categories = $this->order_by('name')->as_array()->get_all();
        ! count($categories) ?: $options += array_column($categories, 'name', 'id');
        return $options;
    }

    /**
     * @param array $data
     * [
     *      'id' => '1',
     *      'name' => 'name',
     * ]
     */
    public function update($data = [], $column_name_where = null, $escape = true)
    {
        if (isset($data['name'])) { $data['slug'] = url_title($data['name'].' '.$data['id'], '-', true); }

        parent::update($data, $column_name_where);
    }
}
