<?php

class Answers_Model extends MY_Model
{
    public $table = 'answers';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->has_one['question'] = ['Questions_Model', 'id', 'question_id'];
        $this->has_one['user'] = ['Users_Model', 'id', 'user_id'];
        $this->load->model('Questions_Model');
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'create' :
                $this->form_validation->set_rules('user_id', lang('user'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('question_id', lang('question'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim', 'required']);
                break;
            case 'update' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('question_id', lang('question'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('user_id', lang('user'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim', 'required']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('question_id', lang('question'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('user_id', lang('user'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim', 'required']);
                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);

                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $params
     * [
     *      'question_id' => '1',
     *      'user_id' => '1',
     *      'content' => 'content'
     * ]
     * @return integer $id
     */
    public function create($data = [])
    {
        $id = $this->insert($data);
        $this->Questions_Model->total_answer_recalculate($data['question_id']);

        return $id;
    }
}
