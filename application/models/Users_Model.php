<?php

class Users_Model extends MY_Model
{
    public $table = 'users';
    public $primary_key = 'id';

    public $user_type_admin = 'admin';
    public $user_type_user = 'user';

    public function __construct()
    {
        parent::__construct();
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'create' :
                $this->form_validation->set_rules('user_type', lang('user_type'), ['trim', 'required']);
                $this->form_validation->set_rules('email', lang('email'), ['trim', 'required', 'valid_email', 'max_length[100]', 'is_unique['.$this->table.'.email]']);
                $this->form_validation->set_rules('password', lang('password'), ['trim', 'required', 'max_length[32]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[100]']);
                $this->form_validation->set_rules('address', lang('address'), ['trim', 'required']);

                $this->form_validation->set_rules('phone_number', lang('phone_number'), ['trim', 'required', 'max_length[16]']);
                break;
            case 'forgot_password' :
                $this->form_validation->set_rules('email', lang('email'), ['trim', 'required', 'valid_email', 'max_length[100]', ['forgot_password_callable', [$this, 'forgot_password_check']]]);
                break;
            case 'reset_password' :
                $this->form_validation->set_rules('password', lang('password'), ['trim', 'required', 'max_length[32]']);
                break;
            case 'sign_in' :
                $this->form_validation->set_rules('email', lang('email'), ['trim', 'required', 'valid_email', 'max_length[100]']);
                $this->form_validation->set_rules('password', lang('password'), ['trim', 'required', 'max_length[32]', ['sign_in_callable', [$this, 'sign_in_check']]]);
                break;
            case 'sign_up' :
                $this->form_validation->set_rules('user_type', lang('user_type'), ['trim', 'required']);
                $this->form_validation->set_rules('email', lang('email'), ['trim', 'required', 'valid_email', 'max_length[100]', 'is_unique['.$this->table.'.email]']);
                $this->form_validation->set_rules('password', lang('password'), ['trim', 'required', 'max_length[32]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[100]']);
                $this->form_validation->set_rules('phone_number', lang('phone_number'), ['trim', 'required', 'max_length[16]']);
                break;
            case 'update' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('user_type', lang('user_type'), ['trim', 'required']);
                $this->form_validation->set_rules('email', lang('email'), ['trim', 'required', 'valid_email', 'max_length[100]', ['email_callable', [$this, 'email_check']]]);
                $this->form_validation->set_rules('password', lang('password'), ['trim', 'max_length[32]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[100]']);

                $this->form_validation->set_rules('address', lang('address'), ['trim', 'required']);
                $this->form_validation->set_rules('phone_number', lang('phone_number'), ['trim', 'required', 'max_length[16]']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('user_type', lang('user_type'), ['trim', 'required']);
                $this->form_validation->set_rules('email', lang('email'), ['trim', 'required', 'valid_email', 'max_length[100]']);
                $this->form_validation->set_rules('password', lang('password'), ['trim', 'required', 'max_length[32]']);
                $this->form_validation->set_rules('name', lang('name'), ['trim', 'required', 'max_length[100]']);

                $this->form_validation->set_rules('address', lang('address'), ['trim', 'required']);
                $this->form_validation->set_rules('phone_number', lang('phone_number'), ['trim', 'required', 'max_length[16]']);
                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);
                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $data
     * [
     *      'user_type' => 'admin' / 'user',
     *      'email' => 'email',
     *      'password' => '******',
     *      'name' => 'name',
     *      'address' => 'address',
     *      'phone_number' => '123456',
     * ]
     * @return integer $id
     */
    public function create($data = [])
    {
        if (isset($data['password'])) { $data['password'] = md5($data['password']); }

        $id = $this->insert($data);
        return $id;
    }

    public function email_check()
    {
        $count = $this->where('id !=', $this->input->post('id'))->where('email', $this->input->post('email'))->count_rows();
        if ($count > 0) {
            $this->form_validation->set_message('email_callable', '{field} '.lang('field_must_contain_a_unique_value').'.');
            return false;
        }
        return true;
    }

    public function forgot_password_check()
    {
        $count = $this->where('email', $this->input->post('email'))->count_rows();
        if ($count == 0) {
            $this->form_validation->set_message('forgot_password_callable', '{field} '.lang('is_not_exist').'.');
            return false;
        }
        return true;
    }

    public function get_user_options()
    {
        $options = ['' => '- '.lang('choose_user').' -'];
        $users = $this->order_by('name')->as_array()->get_all();
        ! count($users) ?: $options += array_column($users, 'name', 'id');
        return $options;
    }

    public function get_user_type_options()
    {
        $options = ['' => '- '.lang('choose_user_type').' -'];
        $user_type = [
            $this->user_type_admin => lang('admin'),
            $this->user_type_user => lang('user'),
        ];
        asort($user_type); $options += $user_type;
        return $options;
    }

    public function sign_in_check()
    {
        $count = $this->where(['email' => $this->input->post('email'), 'password' => md5($this->input->post('password'))])->count_rows();
        if ($count == 0) {
            $this->form_validation->set_message('sign_in_callable', lang('these_credentials_do_not_match_our_records').'.');
            return false;
        }
        return true;
    }

    /**
     * @param array $data
     * [
     *      'id' => '1',
     *      'user_type' => 'admin' / 'user',
     *      'email' => 'email',
     *      'password' => '******',
     *      'name' => 'name',
     *      'address' => 'address',
     *      'phone_number' => '123456',
     * ]
     */
    public function update($data = [], $column_name_where = null, $escape = true)
    {
        if (isset($data['password'])) { $data['password'] = md5($data['password']); }

        parent::update($data, $column_name_where);
    }
}
