<?php

class News_Model extends MY_Model
{
    public $table = 'news';
    public $primary_key = 'id';
    public $delete_cache_on_save = true;

    public function __construct()
    {
        parent::__construct();
        $this->has_one['category'] = ['Categories_Model', 'id', 'category_id'];
        $this->after_get = ['get_image_file_info'];
        $this->before_delete = ['delete_files'];
    }

    public function validate($scenario = '')
    {
        switch ($scenario) {
            case 'create' :
                $this->form_validation->set_rules('category_id', lang('category'), ['trim', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                break;
            case 'update' :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('category_id', lang('category'), ['trim', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                break;
            default :
                $this->form_validation->set_rules('id', lang('id'), ['trim', 'required', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('category_id', lang('category'), ['trim', 'integer', 'max_length[11]']);
                $this->form_validation->set_rules('title', lang('title'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('slug', lang('slug'), ['trim', 'required', 'max_length[255]']);
                $this->form_validation->set_rules('content', lang('content'), ['trim']);

                $this->form_validation->set_rules('image', lang('name'), ['trim', 'max_length[255]']);
                $this->form_validation->set_rules('created_at', lang('created_at'), ['trim', 'required']);
                $this->form_validation->set_rules('updated_at', lang('updated_at'), ['trim', 'required']);
                break;
        }
        return $this->form_validation->run();
    }

    /**
     * @param array $data
     * [
     *      'category_id' => '1',
     *      'title' => 'title',
     *      'content' => 'content',
     *      'image' => 'image',
     * ]
     * @return integer $id
     */
    public function create($data = [])
    {
        if (isset($data['title'])) { $data['slug'] = url_title($data['title'], '-', true); }

        $data['id'] = $id = $this->insert($data);
        $this->update($data, $id);

        return $id;
    }

    public function delete_files($data)
    {
        $path = $this->upload->news_path.'/'.$data['id'];
        delete_files($path, true); // file_helper
        is_dir($path) ? rmdir($path) : '';
    }

    public function file_upload($temp_file = '', $new_path = '')
    {
        $new_file = '';

        if (file_exists($temp_file)) {
            if (! is_dir($new_path)) { mkdir($new_path, 0755, true); }
            $temp_file_info = get_file_info($temp_file);
            $new_file = $new_path.'/'.$temp_file_info['name'];
            rename($temp_file, $new_file);
        }

        foreach (glob($new_path.'/*') as $old_file) {
            if (! in_array(basename($old_file), [basename($new_file)])) { unlink($old_file); }
        }

        return $new_file;
    }

    public function get_image_file_info($data)
    {
        if ($this->is_assoc($data)) {
            $file_info = get_file_info($data['image']);
            $data['image_name'] = $file_info['name'];
            $data['image_size'] = $file_info['size'];
        }
        return $data;
    }

    /**
     * @param array $data
     * [
     *      'id' => '1',
     *      'category_id' => 'category_id',
     *      'title' => 'title',
     *      'content' => 'content',
     *      'image' => 'image',
     * ]
     */
    public function update($data = [], $column_name_where = null, $escape = true)
    {
        if (isset($data['title'])) { $data['slug'] = url_title($data['title'].' '.$data['id'], '-', true); }
        if (isset($data['image'])) { $data['image'] = $this->file_upload($data['image'], $this->upload->news_path.'/'.$data['id']); }

        parent::update($data, $column_name_where);
    }
}
