<?php

class MY_Email extends CI_Email
{
    public $config;

    public function get_config()
    {
        $this->config = [
            'protocol' => $_ENV['EMAIL_PROTOCOL'],
            'smtp_host' => $_ENV['EMAIL_SMTP_HOST'],
            'smtp_user' => $_ENV['EMAIL_SMTP_USER'],
            'smtp_pass' => $_ENV['EMAIL_SMTP_PASSWORD'],
            'smtp_port' => $_ENV['EMAIL_SMTP_PORT'],

            'mailtype' => 'html',
            'newline' => "\r\n",
            'sender_name' => $_ENV['EMAIL_SENDER_NAME'],
            'starttls'  => true,
        ];

        if ($_ENV['EMAIL_GMAIL'] == 'true') {
            $this->config['protocol'] = $_ENV['EMAIL_GMAIL_PROTOCOL'];
            $this->config['smtp_host'] = $_ENV['EMAIL_GMAIL_HOST'];
            $this->config['smtp_user'] = $_ENV['EMAIL_GMAIL_USER'];
            $this->config['smtp_pass'] = $_ENV['EMAIL_GMAIL_PASSWORD'];
            $this->config['smtp_port'] = $_ENV['EMAIL_GMAIL_PORT'];
        }

        return $this->config;
    }
}
