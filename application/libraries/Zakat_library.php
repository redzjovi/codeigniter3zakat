<?php

class Zakat_library
{
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->model('Products_Model');
    }

    public function fitrah($rice_price = 0)
    {
        return ($rice_price * 3.5);
    }

    /**
     * @param integer $total
     * @param integer $irrigation_cost_type '0'/ '1'
     * @return integer $zakat
     */
    public function maal_agriculture($total, $irrigation_cost_type)
    {
        $zakat = 0;

        if ($total >= 653) {
            if ($irrigation_cost_type == 0) { $zakat = $total * 10 / 100; }
            else if ($irrigation_cost_type == 1) { $zakat = $total * 5 / 100; }
        }

        return $zakat;
    }

    /**
     * @param string $code 'cow' / 'lamb'
     * @param integer $owned
     * @return array $zakat
     * [
     *      'description' => '',
     *      'total' => '0',
     * ]
     */
    public function maal_farm_animals($code, $owned)
    {
        $zakat = ['description' => '', 'total' => 0];

        if ($code == 'cow') {
            $male_female = $this->maal_farm_animals_cow_lamb($owned);

            $description = '';
            if ($male_female['male'] > 0) {
                empty($description) ?: $description .= ', ';
                $description .= $male_female['male']." tabi'";
            }
            if ($male_female['female'] > 0) {
                empty($description) ?: $description .= ', ';
                $description .= $male_female['female'].' mussinah';
            }

            $zakat = ['description' => $description, 'total' => array_sum($male_female)];
        } else if ($code == 'lamb') {
            if ($owned >= 40 && $owned <= 120) {
                $zakat['total'] = 1;
            } else if ($owned >= 121 && $owned <= 200) {
                $zakat['total'] = 2;
            } else if ($owned >= 201) {
                $zakat['total'] = ceil($owned / 100);
            }
        }

        return $zakat;
    }

    /**
     * @param integer $total
     * @param integer $male
     * @param integer $female
     * @param integer $male_count
     * @param integer $female_count
     * @return array
     * [
     *      'male' => '0',
     *      'female' => '0'
     * ]
     */
    public function maal_farm_animals_cow_lamb(& $total, $male = 30, $female = 40, & $male_count = 0, & $female_count = 0)
    {
        if ($total >= $female && $total % $female == 0) {
            $count = floor($total / $female);
            $total = $total - ($count * $female);
            $female_count += $count;
            $this->maal_farm_animals_cow_lamb($total, $male, $female, $male_count, $female_count);
        }

        if ($total >= $male && $total % $male == 0) {
            $count = floor($total / $male);
            $total = $total - ($count * $male);
            $male_count += $count;
            $this->maal_farm_animals_cow_lamb($total, $male, $female, $male_count, $female_count);
        }

        if ($total >= $female && $total > 0) {
            $count = 1;
            $total = $total - ($count * $female);
            $female_count += $count;
            $this->maal_farm_animals_cow_lamb($total, $male, $female, $male_count, $female_count);
        };

        if ($total >= $male && $total > 0) {
            $count = 1;
            $total = $total - ($count * $male);
            $male_count += $count;
            $this->maal_farm_animals_cow_lamb($total, $male, $female, $male_count, $female_count);
        }

        return ['male' => $male_count, 'female' => $female_count];
    }

    /**
     * @param integer $owned
     * @return integer $zakat
     */
    public function maal_gold($owned)
    {
        $zakat = 0;
        if ($owned > 84) { $zakat = $owned * 2.5 / 100; }
        return $zakat;
    }

    /**
     * @param integer $money
     * @param integer $nishab_total
     * @return integer $zakat
     */
    public function maal_money($money, $nishab_total)
    {
        $zakat = 0;
        if ($nishab_total > 0 && $money >= $nishab_total) { $zakat = $money * 2.5 / 100; }
        return $zakat;
    }

    /**
     * @param integer $total
     * @return integer $zakat
     */
    public function maal_rikaz($total)
    {
        $zakat = $total * 20 / 100;
        return $zakat;
    }

    /**
     * @param integer $owned
     * @return integer $zakat
     */
    public function maal_silver($owned)
    {
        $zakat = 0;
        if ($owned > 594) { $zakat = $owned * 2.5 / 100; }
        return $zakat;
    }

    public function get_duty_options()
    {
        $duty_options = [
            '0' => lang('not_required'),
            '1' => lang('required'),
        ];
        asort($duty_options);
        return $duty_options;
    }

    public function get_irrigation_cost_type_options()
    {
        $duty_options = [
            '0' => lang('without_cost'),
            '1' => lang('with_cost'),
        ];
        asort($duty_options);
        return $duty_options;
    }

    /**
     * @param integer $total_harvest
     * @return boolean / integer $must_zakat
     */
    public function get_must_zakat_maal_agriculture($total_harvest)
    {
        $must_zakat = 0;
        if ($total_harvest >= 653) { $must_zakat = 1; }
        return $must_zakat;
    }

    public function get_nishab_options()
    {
        $nishab_options = ['' => ''];

        // delete
        // $this->ci->Products_Model->set_return_type('array');
        // $products = $this->ci->Products_Model->where('type', 'zakat_maal_gold_and_silver')->get_all();
        // $nishab_options += array_column($products, 'name', 'name');
        // $nishab_options = array_change_key_case($nishab_options, CASE_LOWER);

        $products = $this->ci->Products_Model->where('type', 'zakat_maal_gold_and_silver')->get_all();
        foreach ($products as $product) {
            $product_name = strtolower($product->name) == 'emas' ? 'gold' : 'silver';
            $nishab_options += [$product_name => $product->name];
        }

        return $nishab_options;
    }

    /**
     * @param string $nishab_type 'gold' / 'silver'
     * @param integer $price
     * @return integer $total
     */
    public function get_nishab_total($nishab_type, $price)
    {
        if ($nishab_type == 'gold') { $total = $price * 85; }
        else if ($nishab_type == 'silver') { $total = $price * 595; }
        else { $total = 0; }

        return $total;
    }
}
