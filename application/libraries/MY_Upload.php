<?php

class MY_Upload extends CI_Upload
{
    public $news_path = 'uploads/news';
    public $pages_path = 'uploads/pages';
    public $products_path = 'uploads/products';
    public $summernote_path = 'uploads/summernote';
    public $temp_path = 'uploads/temp';
    public $transactions_path = 'uploads/transactions';

    public function file_upload($temp_file = '', $new_path = '')
    {
        $new_file = '';

        if (file_exists($temp_file)) {
            if (! is_dir($new_path)) { mkdir($new_path, 0755, true); }
            $temp_file_info = get_file_info($temp_file);
            $new_file = $new_path.'/'.$temp_file_info['name'];
            rename($temp_file, $new_file);
        }

        foreach (glob($new_path.'/*') as $old_file) {
            if (! in_array(basename($old_file), [basename($new_file)])) { unlink($old_file); }
        }

        return $new_file;
    }
}
