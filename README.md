## Synopsis
Zakat with Codeigniter 3.
Demo: [codeigniter3zakat](https://codeigniter3zakat.herokuapp.com/backend/auth/sign_in), admin@email.com, admin

## Included css and js
- [bootstrap](https://github.com/twbs/bootstrap)
- [bootstrap-material-design](https://github.com/mdbootstrap/bootstrap-material-design)
- [bootstrap4c-dropzone](https://github.com/haubek/bootstrap4c-dropzone)
- [bs-autohide-navbar](https://github.com/istvan-ujjmeszaros/bootstrap-autohidingnavbar)
- [dropzone](https://github.com/enyo/dropzone)
- [initial.js](https://github.com/judesfernando/initial.js)
- [jquery-dynatable](https://github.com/alfajango/jquery-dynatable)
- [numeral](https://github.com/adamwdraper/Numeral-js)
- [sticky-kit](https://github.com/leafo/sticky-kit)
- [summernote](https://github.com/summernote/summernote)

## Include core custom
- [CodeIgniter-MY_Model](https://github.com/avenirer/CodeIgniter-MY_Model)

## Included third party
- [codeigniter-forensics](https://github.com/lonnieezell/codeigniter-forensics)

## Installation
- Run command
```
composer install
```
- Copy .env.example to .env, and set all settings
- In database/, import .sql to your database
